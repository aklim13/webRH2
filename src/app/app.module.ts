import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './components/app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthGuard} from './guards/auth.guard';
import {AuthService} from './services/auth.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {JwtInterceptor} from './helpers/JwtInterceptor';
import {AppRoutingModule} from './app-routing.module';
import {RoleGuard} from './guards/role.guard';
import {EmployeesModule} from '@app/components/employees';
import {AuthModule} from '@app/components/auth';
import {NgProgressModule} from '@ngx-progressbar/core';
import {NgProgressHttpModule} from '@ngx-progressbar/http';
import {NgProgressRouterModule} from '@ngx-progressbar/router';
import {CoreModule} from '@app/components/core';
import {PasswordService} from '@app/services/password.service';
import {SidebarService} from '@app/services/sidebar.service';
import {ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {UrlHelperService} from '@app/services/url-helper.service';
import {ProfileModule} from '@app/components/profile';
import {FormService} from '@app/services/form.service';
import {EmpRefreshService} from '@app/services/emp-refresh.service';
import {RefreshProfileService} from '@app/services/refresh-profile.service';
import {ProfileGuard} from '@app/guards/profile.guard';
import {DashboardModule} from '@app/components/dashboard';
import {en_US, NgZorroAntdModule, NZ_I18N} from 'ng-zorro-antd';
import {CongeModule} from '@app/components/conge';
import {AbsenceModule} from '@app/components/absence';
import {SettingsModule} from '@app/components/settings';
import {UtilsService} from '@app/services/utils.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    EmployeesModule,
    DashboardModule,
    CongeModule,
    AbsenceModule,
    ProfileModule,
    AuthModule,
    SettingsModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    NgProgressModule.forRoot(),
    NgZorroAntdModule.forRoot(),
    NgProgressHttpModule,
    NgProgressRouterModule,
    CoreModule
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
    AuthGuard,
    RoleGuard,
    ProfileGuard,
    UtilsService,
    SidebarService,
    AuthService,
    PasswordService,
    UrlHelperService,
    EmpRefreshService,
    RefreshProfileService,
    {provide: NZ_I18N, useValue: en_US},
    FormService,
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}


