import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {EmployeService} from '@app/services/employe.service';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProfileGuard implements CanActivate {
  constructor(private router: Router,
              private employeService: EmployeService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.employeService.getSelectedEmp(next.params['matricule'])
      .mapTo(true)
      .catch(err => {
        this.router.navigateByUrl('/not-found');
        return Observable.of(false);
      });
  }

}
