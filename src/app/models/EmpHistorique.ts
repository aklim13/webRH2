export interface EmpHistorique {
  historiqueIdentity?: HistoriqueIdentity;
  date?: string;
  type?: string;
}

export interface HistoriqueIdentity {
  employe?: number;
  valeur?: string;
}
