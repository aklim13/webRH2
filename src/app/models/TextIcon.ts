export interface TextIcon {
  title?: String;
  icon?: String;
  link?: string;
  subtitle?: TextIcon[];

}
