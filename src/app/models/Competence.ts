export interface Competence {
  comptenceId?: number;
  competence?: string;
  note?: number;
}
