import {Sort} from '../Sort';
import {Pageable} from '../Pageable';
import {Experience} from '../Experience';

export interface ExperienceList {
  content?: Experience[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
