import {Sort} from '@app/models/Sort';
import {Pageable} from '@app/models/Pageable';
import {Competence} from '@app/models/Competence';

export interface CompetenceList {
  content?: Competence[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
