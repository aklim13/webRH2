import {Sort} from '@app/models/Sort';
import {Pageable} from '@app/models/Pageable';
import {Contact} from '@app/models/Contact';

export interface ContactList {
  content?: Contact[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
