import {Pageable} from '@app/models/Pageable';
import {Sort} from '@app/models/Sort';

export interface AbsenceList {
  content?: Absence[];
  pageable?: Pageable;
  totalPages?: number;
  totalElements?: number;
  last?: boolean;
  size?: number;
  number?: number;
  sort?: Sort;
  first?: boolean;
  numberOfElements?: number;
}

export interface Absence {
  fullname?: string;
  raison?: string;
  lien?: string;
  dateAbsence?: string;
  dateJustification?: string;
  documentPresent?: boolean;
}
