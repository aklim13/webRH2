import {Pageable} from '../Pageable';
import {Sort} from '../Sort';
import {EmployePreviewData} from '../EmployePreviewData';

export interface EmployeList {
  content?: EmployePreviewData[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
