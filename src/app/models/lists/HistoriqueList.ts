import {Sort} from '@app/models/Sort';
import {Pageable} from '@app/models/Pageable';
import {EmpHistorique} from '@app/models/EmpHistorique';

export interface HistoriqueList {
  content?: EmpHistorique[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
