import {Sort} from '@app/models/Sort';
import {Pageable} from '@app/models/Pageable';
import {Document} from '@app/models/Document';

export interface DocumentList {
  content?: Document[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
