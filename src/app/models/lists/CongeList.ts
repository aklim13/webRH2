import {Pageable} from '../Pageable';
import {Sort} from '../Sort';

export interface CongeList {
  content?: CongeDemande[];
  pageable?: Pageable;
  totalElements?: number;
  totalPages?: number;
  last?: boolean;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}

export interface CongeDemande {
  status?: string;
  fullName?: string;
  sum?: number;
  raison?: string;
  dateFin?: string;
  dateDebut?: string;
  congeId?: number;
}

export interface CongeDemander {
  typeCongeId?: number;
  raison?: string;
  dateDebut?: string;
  dateFin?: string;
}
