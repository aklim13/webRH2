export interface LanguageLevel {
  languageLevelId?: number;
  language?: string;
  niveauOrale?: string;
  niveauEcrit?: string;
}
