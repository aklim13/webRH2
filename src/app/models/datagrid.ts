export interface DataGridOptions {
  onClick(event): void;

  onPage(event): void;

  onSelect({selected}): void;
}
