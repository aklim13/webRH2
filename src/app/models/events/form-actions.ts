export interface FormActions {
  onChange();

  onSelect(selected: {});

  onSearch(query: string);

  onReport();

  onDelete();

  onEdit();

  onAdd();
}
