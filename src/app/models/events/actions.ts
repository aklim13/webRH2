export interface OnAdd {
  onAdd();
}

export interface OnDelete {
  onDelete();
}

export interface OnEdit {
  onEdit();
}

export interface OnMode {
  onMode();
}

export interface OnReport {
  onReport();
}

export interface OnSearch {
  onSearch(query: string);
}

export interface OnSelect {
  onSelect(selected: {});
}

export interface OnChange {
  onChange();
}
