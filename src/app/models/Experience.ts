export interface Experience {
  experienceId?: number;
  description?: string;
  dureeMois?: string;
  lieu?: string;
  note?: number;
}
