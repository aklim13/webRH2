export interface Language {
  langueId: number;
  language: string;
}
