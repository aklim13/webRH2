export interface DataTable {
  sexe: String[];
  postes?: Poste[];
  echelles?: Echelle[];
  echellons?: Echellon[];
  grades?: Grade[];
  services?: Service[];
  statusFamilles: StatusFamille[];
  roles?: Role[];
}

export interface Echelle {
  echelleId?: number;
  echelle?: number;
}

export interface Echellon {
  echellonId?: number;
  echellon?: number;
}

export interface Poste {
  posteId?: number;
  poste?: string;
  salaire?: number;
}

export interface Role {
  roleId?: number;
  role?: string;
}

export interface StatusFamille {
  statusId?: number;
  status?: string;
}

export interface Grade {
  gradeId?: number;
  grade: string;
}

export interface Service {
  serviceId?: number;
  service: string;
}
