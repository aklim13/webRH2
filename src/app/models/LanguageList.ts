import {Sort} from '@app/models/Sort';
import {Pageable} from '@app/models/Pageable';
import {LanguageLevel} from '@app/models/LanguageLevel';

export interface LanguageList {
  content?: LanguageLevel[];
  pageable?: Pageable;
  totalElements?: number;
  last?: boolean;
  totalPages?: number;
  size?: number;
  number?: number;
  sort?: Sort;
  numberOfElements?: number;
  first?: boolean;
}
