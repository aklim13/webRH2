export interface EmployePreviewData {
  matricule?: number;
  prenom?: string;
  nom?: string;
  email?: string;
  cin?: string;
  image?: string;
  note?: number;
  poste?: string;
  admin?: boolean;
}
