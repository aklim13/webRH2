export interface EmployeData {
  adresse?: string;
  cin?: string;
  nom?: string;
  prenom?: string;
  sexe?: string;
  email?: string;
  admin?: boolean,
  dateRecrutement?: string;
  salaireBase?: number;
  nombreEnfant?: number;
  statusFamilleId?: number;
  echelle?: number;
  echellon?: number;
  gradeId?: number;
  posteId?: number;
  serviceId?: number;
  mutuel?: string;
  nbancaire?: string;
  prenomConjoint?: string;
  nomConjoint?: string;
  codePostal?: string;
  tel?: string;
  dateNaissance?: string;
}
