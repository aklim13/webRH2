interface CongeStats {
  congeData?: Array<number>;
  congeLabels?: Array<String>;

}

interface AbsenceStats {
  absenceLabels?: Array<String>;
  absenceData?: Array<number>;
}
