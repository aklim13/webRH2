export enum TypeDocumentEnum {
  CV,
  CERTIFICAT_MEDICAL,
  BAC,
  CIN,
  LICENCE,
  DUT,
  DOCTORAT,
  MASTER,
  PHOTO
}
