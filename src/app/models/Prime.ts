export interface Prime {
  primeId?: number;
  prime?: string;
  plafond?: string;
  condition?: string;
}
