export interface LanguageLevelDTO {
  languageLevelId?: number;
  languageId?: number;
  niveauOraleId?: number;
  niveauEcritId?: number;
}
