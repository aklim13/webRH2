import {StatusFamille} from '@app/models/DataTable';

export interface EmployeEditData {
  matricule?: number;
  cin?: string;
  nom?: string;
  prenom?: string;
  sexe?: string;
  email?: string;
  anciennite?: string;
  dateRecrutement?: string;
  dateNaissance?: string;
  poste?: String;
  grade?: String;
  service?: String;
  echelle?: number;
  echellon?: number;
  admin?: boolean;
  salaireBase?: number;
  nombreEnfant?: number;
  statusFamille?: StatusFamille;
  note?: number;
  adresse?: string;
  tel?: string;
  codePostal?: number;
  linkedinLink?: string;
  nmutuel?: string;
  nbancaire?: string;
  nomConjoint?: string;
  prenomConjoint?: string;
  lien?: string;
}
