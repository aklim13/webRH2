export interface FormButton {
  title: String;
  id?: string;
  iconName: String;
}
