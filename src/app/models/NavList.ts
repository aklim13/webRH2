import {TextIcon} from '@app/models/TextIcon';

export interface NavList {
  open?: boolean;
  title?: String;
  icon?: String;
  hide?: boolean;
  mono?: boolean;
  link?: String;
  subtitle?: TextIcon[];
}
