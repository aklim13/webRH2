export interface Statistique {
  congeData?: number[];
  congeLabels?: string[];
  absenceData?: number[];
  absenceLabels?: string[];
  gradeData?: number[];
  gradeLabels?: string[];
  serviceData?: number[];
  serviceLabels?: string[];
  echelleData?: number[];
  echelleLabels?: string[];
  echellonsData?: number[];
  echellonsLabels?: string[];
  statsFamilleLabel?: string[];
  statsFamilleData?: number[];
  posteLabels?: string[];
  posteData?: number[];
  sexeLabel?: string[];
  sexeData?: number[];
  salaireLabel?: string[];
  salaireData?: number[];
  totalPrime?: number;
  salaireMoy?: number;
  ageMoy?: number;
  ancienniteAnneeMoy?: number;
  noteMoyenne?: number;
  posteCount?: number;
  nombreEmploye?: number;
}

export interface SingleStats {
  congeLabel?: string[];
  absenceLabel?: string[];
  absenceData?: number[];
  congeData?: number[];
  primeCount?: number;
  competneceCount?: number;
  experienceCount?: number;
  anciennite?: string;
  totalConge?: number;
  totalAbsence?: number;
  salaire?: number;
}
