export interface PasswordReq {
  newPassword: string;
  confirmPassword: string;
  oldPassword: string;
}
