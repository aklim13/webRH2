import {TypeDocument} from '@app/models/TypeDocument';

export interface Document {
  documentId?: number;
  titre?: string;
  lien?: string;
  origin?: string;
  typeDocument?: TypeDocument;
  note?: number;
  createdAt?: string;
  updatedAt?: string;
}
