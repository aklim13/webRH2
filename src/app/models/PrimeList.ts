import {Pageable} from '@app/models/Pageable';
import {Prime} from '@app/models/Prime';
import {Sort} from '@app/models/Sort';

export interface PrimeList {
  content?: Prime[];
  pageable?: Pageable;
  totalElements?: number;
  totalPages?: number;
  last?: boolean;
  size?: number;
  number?: number;
  sort?: Sort;
  first?: boolean;
  numberOfElements?: number;
}
