export interface Contact {
  contactId?: number;
  nom?: string;
  prenom?: string;
  tel?: string;
  email?: string;
}
