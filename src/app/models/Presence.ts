export interface Presence {
  fullName?: string;
  poste?: string;
  matricule?: number;
  lien?: string;
}
