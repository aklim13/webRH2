export interface TypeDocument {
  typeDocumentId?: number;
  typeDocument?: string;
  description?: string;
}
