import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileLangComponent} from './profile-lang.component';

describe('ProfileLangComponent', () => {
  let component: ProfileLangComponent;
  let fixture: ComponentFixture<ProfileLangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileLangComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileLangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
