import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {ProfileService} from '@app/services/profile.service';
import {ActivatedRoute} from '@angular/router';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {FormService} from '@app/services/form.service';
import {LanguageService} from '@app/services/language.service';
import {LanguageLevelDTO} from '@app/models/LanguageLevelDTO';
import {OnAdd, OnChange, OnDelete, OnEdit, OnReport} from '@app/models/events/actions';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-lang',
  templateUrl: './profile-lang.component.html',
  styleUrls: ['./profile-lang.component.sass']
})
export class ProfileLangComponent implements OnInit, OnDestroy, OnAdd, OnDelete, OnEdit, OnChange, OnReport {
  rowButtonType = RowButtonType.ADD_DELETE_EDIT_REPORT;
  tableType = TableType.ProfileLanguage;
  showForm = {add: false, edit: false, delete: false};
  selectedLanguages: LanguageLevelDTO[] = [];
  language: LanguageLevelDTO;

  @ViewChild('datatable') dataTable;

  columns: TableColumn[] = [
    {prop: 'languageLevelId', name: 'ID', width: 50},
    {prop: 'language', name: 'Langue'},
    {prop: 'niveauOrale', name: 'orale'},
    {prop: 'niveauEcrit', name: 'ecrit'}
  ];

  constructor(private profileService: ProfileService,
              private formService: FormService,
              private route: ActivatedRoute,
              private message: NzMessageService,
              private languageService: LanguageService,
              private router: ActivatedRoute) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  onSelect(s): void {
    this.selectedLanguages = s.selected;
    if (s.selected.length > 0) {
      this.language = s.selected.slice(-1)[0];
    }
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onDelete() {
    if (this.selectedLanguages.length > 0) {
      this.languageService.deleteUserLanguage(this.route.snapshot.params['matricule']
        , this.selectedLanguages.map(l => l.languageLevelId))
        .subscribe(
          res => {
          }, err => {
            console.log(err);
          }, () => {
            this.message.success('Language(s) ajouté');
            this.showForm.delete = true;
            this.refresh();
          }
        );
    }
  }

  onReport() {
  }

  onEdit() {
    if (this.selectedLanguages.length === 0) {
      // hide the form when nothing selected;
      if (this.showForm.edit) {
        this.showForm.edit = false;
        return;
      }
      this.message.info('Aucun language selectionnée');
    } else {
      this.formService.showEditForm(this.showForm);
    }
  }

  onChange() {
    this.refresh();
  }

  refresh() {
    this.dataTable.onPage({offset: 0});
  }

}
