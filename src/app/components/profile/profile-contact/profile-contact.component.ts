import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProfileService} from '@app/services/profile.service';
import {TableType} from '@app/models/enums/TableType';
import {TableColumn} from '@swimlane/ngx-datatable';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {FormService} from '@app/services/form.service';
import {Contact} from '@app/models/Contact';
import {ContactService} from '@app/services/contact.service';
import {FormActions} from '@app/models/events/form-actions';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-contact',
  templateUrl: './profile-contact.component.html',
  styleUrls: ['./profile-contact.component.sass']
})
export class ProfileContactComponent implements OnInit, OnDestroy, FormActions {
  contact: Contact;
  selectedContacts = [];
  showForm = {add: false, edit: false};
  tabletype = TableType.ProfileContact;
  @ViewChild('datatable') dataTable;
  rowButtonType = RowButtonType.ADD_DELETE_EDIT_REPORT;
  columns: TableColumn[] = [
    {prop: 'contactId', name: 'ID', width: 50},
    {prop: 'prenom'},
    {prop: 'nom'},
    {prop: 'tel', name: 'Téléphone'}
  ];
  query: string;

  constructor(private route: ActivatedRoute,
              private formService: FormService,
              private message: NzMessageService,
              private contactService: ContactService,
              private profileService: ProfileService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  onSelect(s): void {
    this.selectedContacts = s.selected;
    if (s.selected.length > 0) {
      this.contact = s.selected.slice(-1)[0];
    }
  }

  onEdit() {
    if (this.selectedContacts.length === 0) {
      // hide the form when nothing selected;
      if (this.showForm.edit) {
        this.showForm.edit = false;
        return;
      }
      this.message.warning('Aucun contact selectionnée');
    } else {
      this.formService.showEditForm(this.showForm);
    }
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onChange() {
    this.refresh();
  }

  onDelete() {
    if (this.selectedContacts.length > 0) {
      this.contactService.deleteEmploye(this.route.snapshot.params['matricule']
        , this.selectedContacts.map((c: Contact) => c.contactId))
        .subscribe(
          res => {
          }, err => {
          }, () => {
            this.refresh();
          }
        );
    }
  }

  refresh() {
    this.dataTable.onPage({offset: 0});
  }

  onSearch(query: string) {
    this.query = query;
  }

  onReport() {
  }
}
