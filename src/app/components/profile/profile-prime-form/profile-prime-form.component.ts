import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {FormService} from '@app/services/form.service';
import {Prime} from '@app/models/Prime';
import {PrimeService} from '@app/services/prime.service';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-prime-form',
  templateUrl: './profile-prime-form.component.html',
  styleUrls: ['./profile-prime-form.component.sass']
})
export class ProfilePrimeFormComponent implements OnInit, OnChanges, OnDestroy {
  primeForm: FormGroup;
  matricule: number;
  allPrimes: Prime[] = [];
  @Input() primeData: Prime;
  @Input() add: Boolean;
  @Input() delete: Boolean;
  @Input() edit: Boolean;
  @Output() primeChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private router: ActivatedRoute,
              private message: NzMessageService,
              private primeService: PrimeService,
              public formService: FormService) {
  }

  ngOnInit() {
    this.matricule = this.router.snapshot.params['matricule'];
    this.primeForm = this.fb.group(
      {
        prime: ['', Validators.required]
      }
    );
    this.refreshSelect();
  }

  onFormSubmit() {
    if (this.add) {
      this.primeService.postPrime(this.matricule, ((this.primeForm.get('prime').value) as Prime).primeId)
        .pipe(untilComponentDestroyed(this))
        .subscribe(
          res => {

          }, err => {
            console.log(err);
          }, () => {
            this.refreshSelect();
            this.message.success('Prime a été ajouté avec success.');
            this.primeChanged.emit();
          }
        );
    }
    if (this.edit) {
      this.primeService.updatePrime(this.matricule, ((this.primeForm.get('prime').value) as Prime).primeId, this.primeData.primeId)
        .pipe(untilComponentDestroyed(this))
        .subscribe(
          res => {
          }, err => {
            console.log(err);
          }, () => {
            this.refreshSelect();
            this.message.success('Prime a été modifié avec success.');
            this.primeChanged.emit();
          }
        );
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.edit || this.add) {
      this.primeForm.setValue({prime: this.allPrimes[0]});
    }
    if (this.delete) {
      this.refreshSelect();
      this.delete = false;
    }
  }

  ngOnDestroy(): void {
  }

  refreshSelect() {
    this.primeService.getPrimes(this.matricule).subscribe(
      primes => {
        this.allPrimes = primes;
        this.primeForm.setValue({prime: this.allPrimes[0]});
      },
      err => {

      },
      () => {
      }
    );
  }
}
