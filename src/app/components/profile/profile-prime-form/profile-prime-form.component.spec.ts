import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfilePrimeFormComponent} from './profile-prime-form.component';

describe('ProfilePrimeFormComponent', () => {
  let component: ProfilePrimeFormComponent;
  let fixture: ComponentFixture<ProfilePrimeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfilePrimeFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePrimeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
