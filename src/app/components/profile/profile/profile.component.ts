import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SidebarService} from '@app/services/sidebar.service';
import {EmployeService} from '@app/services/employe.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  constructor(public router: Router,
              private employeService: EmployeService,
              public route: ActivatedRoute,
              public sidebarService: SidebarService) {
  }

  ngOnInit() {

  }

}
