import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {OnSearch} from '@app/models/events/actions';
import {RefreshProfileService} from '@app/services/refresh-profile.service';

@Component({
  selector: 'app-profile-historique',
  templateUrl: './profile-historique.component.html',
  styleUrls: ['./profile-historique.component.sass']
})
export class ProfileHistoriqueComponent implements OnInit, OnDestroy, OnSearch {
  rowButtonType = RowButtonType.NO_BUTTON;
  tableType = TableType.ProfileHistorique;
  @ViewChild('dataTable') table;
  query: string;
  columns: TableColumn[] = [
    {prop: 'valeur', name: 'Changement'},
    {prop: 'type', name: 'Nature'},
    {prop: 'date'},
  ];


  constructor(private refreshProfile: RefreshProfileService) {
  }

  ngOnInit() {
    this.refreshProfile.refreshHistory.subscribe(
      res => {
        this.refresh();
      });
  }

  ngOnDestroy() {

  }

  onSearch(query) {
    this.query = query;
  }

  refresh() {
    try {
      this.table.onPage({offset: 0});
    } catch (e) {
    }
  }
}
