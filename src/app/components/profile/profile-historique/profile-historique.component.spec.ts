import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileHistoriqueComponent} from './profile-historique.component';

describe('ProfileHistoriqueComponent', () => {
  let component: ProfileHistoriqueComponent;
  let fixture: ComponentFixture<ProfileHistoriqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileHistoriqueComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileHistoriqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
