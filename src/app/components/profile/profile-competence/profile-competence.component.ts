import {Component, OnDestroy, OnInit} from '@angular/core';
import {Competence} from '@app/models/Competence';
import {ActivatedRoute} from '@angular/router';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {CompetenceService} from '@app/services/competence.service';
import {FormService} from '@app/services/form.service';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {OnAdd, OnChange, OnEdit} from '@app/models/events/actions';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-competence',
  templateUrl: './profile-competence.component.html',
  styleUrls: ['./profile-competence.component.sass']
})
export class ProfileCompetenceComponent implements OnInit, OnAdd, OnEdit, OnChange, OnDestroy {
  showForm = {add: false, delete: false};
  competences: Competence[];
  rowButtonType = RowButtonType.ADD_DELETE_EDIT_REPORT;
  competencesToDelete = [];
  matricule: number;

  constructor(private competenceService: CompetenceService,
              private formService: FormService,
              private message: NzMessageService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.matricule = params['matricule'];
        this.refreshCompetences();
      }
    );
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onEdit() {
    this.showForm.delete = !this.showForm.delete;
    if (!this.showForm.delete) {
      this.competenceService.deleteCompetence(this.matricule, this.competencesToDelete)
        .pipe(untilComponentDestroyed(this))
        .subscribe(
          res => {
          },
          err => {
          },
          () => {
            this.refreshCompetences();
            this.message.info('compétence modifié avec success.');
          }
        );
    }
  }

  refreshCompetences() {
    this.competenceService.getUserCompetences(this.matricule).subscribe(
      competences => {
        this.competences = competences;
      },
      err => {
      },
      () => {
      }
    );
  }

  onDelete(competence: Competence) {
    const index = this.competences.indexOf(competence);
    if (index >= 0) {
      this.competencesToDelete.push(competence.comptenceId);
      this.competences.splice(index, 1);
    }
  }

  ngOnDestroy(): void {
  }

  onChange() {
    this.refreshCompetences();
  }
}
