import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormService} from '@app/services/form.service';
import {Contact} from '@app/models/Contact';
import {ContactService} from '@app/services/contact.service';
import {ActivatedRoute} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-contact-form',
  templateUrl: './profile-contact-form.component.html',
  styleUrls: ['./profile-contact-form.component.sass']
})
export class ProfileContactFormComponent implements OnInit, OnChanges {
  @Input() contactData: Contact;
  @Input() add: Boolean;
  @Input() edit: Boolean;
  @Output() contactChanged: EventEmitter<any> = new EventEmitter<any>();

  contactFormGroup: FormGroup;
  matricule: number;

  constructor(private fb: FormBuilder,
              private router: ActivatedRoute,
              private message: NzMessageService,
              private contactService: ContactService,
              public formService: FormService) {
  }

  ngOnInit() {
    this.matricule = this.router.snapshot.params['matricule'];
    this.createContactForm();
  }

  createContactForm() {
    this.contactFormGroup = this.fb.group(
      {
        nom: ['', Validators.required],
        prenom: ['', Validators.required],
        tel: ['', Validators.compose([Validators.maxLength(10), Validators.required])],
        email: ['', Validators.compose([Validators.email, Validators.required])],
      }
    );
  }

  getContact(): Contact {
    return {
      contactId: this.contactData ? this.contactData.contactId : 0,
      email: this.contactFormGroup.get('email').value,
      tel: this.contactFormGroup.get('tel').value,
      prenom: this.contactFormGroup.get('prenom').value,
      nom: this.contactFormGroup.get('nom').value,
    };
  }

  onFormSubmit() {
    if (this.add) {
      this.contactService.addContact(this.getContact(), this.matricule).subscribe(
        contact => {
        },
        err => {
        },
        () => {
          this.message.success('Contact a été ajouté avec success.');
          this.contactChanged.emit();
        }
      );
    }
    if (this.edit) {
      this.contactService.updateContact(this.getContact(), this.matricule).subscribe(
        contact => {
        },
        err => {
        },
        () => {
          this.message.success('Contact a été modifié avec success.');
          this.contactChanged.emit();
        }
      );
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.contactData && this.edit) {
      this.contactFormGroup = this.fb.group(
        {
          nom: [this.contactData.nom, Validators.required],
          prenom: [this.contactData.prenom, Validators.required],
          tel: [this.contactData.tel, Validators.compose([Validators.maxLength(10), Validators.required])],
          email: [this.contactData.email, Validators.compose([Validators.email, Validators.required])],
        }
      );
    }
  }
}
