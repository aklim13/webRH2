import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {Document} from '@app/models/Document';
import {FormService} from '@app/services/form.service';
import {ActivatedRoute} from '@angular/router';
import {DocumentService} from '@app/services/document.service';
import {HttpClient, HttpEventType, HttpResponse} from '@angular/common/http';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-document-form',
  templateUrl: './profile-document-form.component.html',
  styleUrls: ['./profile-document-form.component.sass']
})
export class ProfileDocumentFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input() add: boolean;
  @Input() edit: boolean;
  @Input() selectedDocument: Document;
  @Output() documentChange: EventEmitter<any> = new EventEmitter<any>();

  uploadedFiles = [];

  progress = 0;
  forms = ['BAC', 'CIN', 'Doctorat', 'Licence', 'Master', 'Dut', 'Photo', 'CV'];
  reset = 1;

  constructor(private http: HttpClient,
              private router: ActivatedRoute,
              private message: NzMessageService,
              private documentService: DocumentService,
              public formService: FormService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnDestroy(): void {
  }

  onFormSubmit() {
    this.documentService.uploadDocument(this.uploadedFiles).subscribe(
      res => {
        console.log(res);
        if (res.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * res.loaded / res.total);
        } else if (event instanceof HttpResponse) {
          this.message.success('Document ajouté');
        }
      },
      err => {
        this.message.error('Erreur fichier pas envoyé au serveur.');
      },
      () => {
        this.progress = 0;
        this.documentChange.emit();
        this.onResetClicked();
      }
    );
  }

  onFileContent(event: any) {
    if (event.reset) {
      this.onResetClicked();
    } else {
      this.uploadedFiles.push(event);
    }
  }

  onResetClicked() {
    this.reset++;
    this.uploadedFiles = [];
  }
}
