import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileDocumentFormComponent} from './profile-document-form.component';

describe('ProfileDocumentFormComponent', () => {
  let component: ProfileDocumentFormComponent;
  let fixture: ComponentFixture<ProfileDocumentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileDocumentFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileDocumentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
