import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileExpComponent} from './profile-exp.component';

describe('ProfileExpComponent', () => {
  let component: ProfileExpComponent;
  let fixture: ComponentFixture<ProfileExpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileExpComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileExpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
