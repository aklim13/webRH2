import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {ProfileService} from '@app/services/profile.service';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {ContactService} from '@app/services/contact.service';
import {FormService} from '@app/services/form.service';
import {Experience} from '@app/models/Experience';
import {ExperienceService} from '@app/services/experience.service';
import {FormActions} from '@app/models/events/form-actions';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-exp',
  templateUrl: './profile-exp.component.html',
  styleUrls: ['./profile-exp.component.sass']
})
export class ProfileExpComponent implements OnInit, OnDestroy, FormActions {
  rowButtonType = RowButtonType.ADD_DELETE_EDIT_REPORT;
  showForm = {add: false, delete: false, edit: false};
  selectedExperience: Experience[] = [];
  experience: Experience;
  tableType = TableType.ProfileExp;
  @ViewChild('datatable') datatable;
  query: string;
  columns: TableColumn[] = [
    {prop: 'experienceId', name: 'ID', width: 50},
    {prop: 'dureeMois', name: 'Durée(Mois)'},
    {prop: 'description', name: 'Description'},
    {prop: 'lieu', name: 'Lieu'},
    {prop: 'note', name: 'Note', width: 50},
  ];

  constructor(private router: ActivatedRoute,
              private profileService: ProfileService,
              private route: ActivatedRoute,
              private formService: FormService,
              private experienceService: ExperienceService,
              private message: NzMessageService,
              private contactService: ContactService,
  ) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onSelect(s): void {
    this.selectedExperience = s.selected;
    if (s.selected.length > 0) {
      this.experience = s.selected.slice(-1)[0];
    }
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onDelete() {
    if (this.selectedExperience.length > 0) {
      this.experienceService.deleteExperience(this.route.snapshot.params['matricule']
        , this.selectedExperience.map(e => e.experienceId))
        .subscribe(
          res => {
          }, err => {
          }, () => {
            this.refresh();
          }
        );
    }
  }

  onEdit() {
    if (this.selectedExperience.length === 0) {
      // hide the form when nothing selected;
      if (this.showForm.edit) {
        this.showForm.edit = false;
        return;
      }
      this.message.info('Aucun expérience selectionnée');
    } else {
      this.formService.showEditForm(this.showForm);
    }
  }

  onChange() {
    this.refresh();
  }

  refresh() {
    this.datatable.onPage({offset: 0});
  }

  onSearch(query: string) {
    this.query = query;
  }

  onReport() {
  }
}
