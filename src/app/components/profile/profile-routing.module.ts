import {NgModule} from '@angular/core';
import {AuthGuard} from '@app/guards/auth.guard';
import {RoleGuard} from '@app/guards/role.guard';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from '@app/components/profile/profile/profile.component';
import {ProfileGuard} from '@app/guards/profile.guard';


const profileRoute: Routes = [
  {
    path: 'profile/:matricule',
    component: ProfileComponent,
    canActivate: [AuthGuard, RoleGuard, ProfileGuard],
    data: {roles: ['admin', 'employe']}
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(profileRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class ProfileRoutingModule {
}
