import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Competence} from '@app/models/Competence';
import {CompetenceService} from '@app/services/competence.service';
import {ActivatedRoute} from '@angular/router';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-competence-form',
  templateUrl: './profile-competence-form.component.html',
  styleUrls: ['./profile-competence-form.component.sass']
})
export class ProfileCompetenceFormComponent implements OnInit, OnChanges, OnDestroy {
  @Input() add: boolean;
  @Input() delete: boolean;
  @Output() competenceChanged: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('select') select;
  matricule: number;
  competences: Competence[] = [];
  selectedCompetences: Competence[] = [];

  constructor(private route: ActivatedRoute,
              private message: NzMessageService,
              private competenceService: CompetenceService) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.matricule = params['matricule'];
        this.refreshSelect();
      }
    );
  }

  onFormSubmit() {
    if (this.add) {
      console.log(this.selectedCompetences.map(c => c.comptenceId));
      this.competenceService.postCompetence(this.matricule, this.selectedCompetences.map(c => c.comptenceId))
        .pipe(untilComponentDestroyed(this)).subscribe(
        res => {
        },
        err => {
          console.log(err);
        },
        () => {
          this.refreshSelect();
          this.message.success('Compétence selectionnée ajouté avec success.');
          this.competenceChanged.emit();
        }
      );
    }
    if (this.delete) {

    }
  }

  ngOnDestroy(): void {
  }

  refreshSelect() {
    this.competenceService.getCompetences(this.matricule).pipe(untilComponentDestroyed(this)).subscribe(
      res => {
        this.competences = res;
        this.selectedCompetences = [];
      }, err => {
      }, () => {
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {

  }
}
