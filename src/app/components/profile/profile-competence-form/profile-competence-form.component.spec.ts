import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileCompetenceFormComponent} from './profile-competence-form.component';

describe('ProfileCompetenceFormComponent', () => {
  let component: ProfileCompetenceFormComponent;
  let fixture: ComponentFixture<ProfileCompetenceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileCompetenceFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCompetenceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
