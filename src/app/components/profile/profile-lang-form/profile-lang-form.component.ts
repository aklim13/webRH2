import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {FormService} from '@app/services/form.service';
import {LanguageService} from '@app/services/language.service';
import {Niveau} from '@app/models/Niveau';
import {Language} from '@app/models/Language';
import {LanguageLevelDTO} from '@app/models/LanguageLevelDTO';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-lang-form',
  templateUrl: './profile-lang-form.component.html',
  styleUrls: ['./profile-lang-form.component.sass']
})
export class ProfileLangFormComponent implements OnInit {
  languageForm: FormGroup;
  matricule: number;

  languages: Language[] = [];
  levels: Niveau[] = [];

  @Input() add: boolean;
  @Input() edit: boolean;
  @Input() selectedLanguage: LanguageLevelDTO;
  @Output() languageChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private router: ActivatedRoute,
              private message: NzMessageService,
              private languageService: LanguageService,
              public formService: FormService) {
  }

  ngOnInit() {
    this.matricule = this.router.snapshot.params['matricule'];
    this.createLanguageForm();
    this.languageService.getLevels().subscribe(
      res => {
        this.levels = res;
      },
      err => {
      },
      () => {

      }
    );
    this.refreshSelect();
  }

  getLanguage() {
    return {
      languageLevelId: this.edit ? this.selectedLanguage.languageLevelId : 0,
      languageId: this.languageForm.get('language').value.langueId,
      niveauOraleId: this.languageForm.get('niveauOrale').value.niveauLangueId,
      niveauEcritId: this.languageForm.get('niveauEcrit').value.niveauLangueId
    };
  }

  onFormSubmit() {
    if (this.add) {
      this.languageService.addLanguageToUser(this.getLanguage(), this.matricule).subscribe(
        res => {
        }, err => {
        }, () => {
          this.refreshSelect();
          this.message.success('Language a été ajouté avec success.');
          this.languageChanged.emit();
        }
      );
    }
    if (this.edit) {
      this.languageService.updateUserLanguage(this.getLanguage(), this.matricule).subscribe(
        res => {
        }, err => {
        }, () => {
          this.refreshSelect();
          this.message.success('Language a été modifié avec success.');
          this.languageChanged.emit();
        }
      );
    }
  }

  private createLanguageForm() {
    this.languageForm = this.fb.group({
      language: [, Validators.required],
      niveauEcrit: [, Validators.required],
      niveauOrale: [, Validators.required]
    });
  }

  private refreshSelect() {
    this.languageService.getUserLanguages(this.matricule).subscribe(
      res => {
        this.languages = res;
        this.languageForm.setValue({
          language: this.languages[0],
          niveauEcrit: this.levels[0],
          niveauOrale: this.levels[0]
        });
      },
      err => {
      },
      () => {
      }
    );
  }
}
