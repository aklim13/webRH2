import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileLangFormComponent} from './profile-lang-form.component';

describe('ProfileLangFormComponent', () => {
  let component: ProfileLangFormComponent;
  let fixture: ComponentFixture<ProfileLangFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileLangFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileLangFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
