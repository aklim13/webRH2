import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfileService} from '@app/services/profile.service';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {EmployeEditData} from '@app/models/EmployeEditData';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {EmpRefreshService} from '@app/services/emp-refresh.service';
import {OnDelete, OnEdit, OnReport} from '@app/models/events/actions';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {EmployeService} from '@app/services/employe.service';
import {RefreshProfileService} from '@app/services/refresh-profile.service';
import {UserService} from '@app/services/user.service';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.sass']
})
export class ProfileInfoComponent implements OnInit, OnDestroy, OnDelete, OnReport, OnEdit {
  editMode = false;
  user: EmployeEditData;
  rowButtonType;
  matricule: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private message: NzMessageService,
              private location: Location,
              private employeService: EmployeService,
              private userService: UserService,
              private modal: NzModalService,
              private refreshProfile: RefreshProfileService,
              private refreshService: EmpRefreshService,
              private profileService: ProfileService) {
    this.rowButtonType = this.userService.isAdmin() ? RowButtonType.EDIT_DELETE_REPORT : RowButtonType.NO_BUTTON;
  }

  ngOnInit() {
    this.refresh();
    this.refreshProfile.refreshInfo.pipe(untilComponentDestroyed(this)).subscribe(
      m => {
        this.refresh();
      }
    );
    this.refreshService.currentMessage.pipe(untilComponentDestroyed(this)).subscribe(
      m => {
        this.refresh();
        this.editMode = false;
      }
    );

  }

  ngOnDestroy(): void {
  }

  onEdit() {
    this.showForm();
  }

  showForm() {
    this.editMode = !this.editMode;
  }

  copyLink(text: string) {
    const event = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', text);
      e.preventDefault();
      document.removeEventListener('copy', event);
    };
    document.addEventListener('copy', event);
    document.execCommand('copy');
    this.message.info('Email copié');
  }

  onDelete() {
    this.showConfirm();
  }

  onReport() {
  }

  showConfirm(): void {
    this.modal.confirm({
      nzOkText: 'Supprimer',
      nzIconType: 'warning',
      nzCancelText: 'Anuller',
      nzTitle: `Voulez vous supprimer tout les informations de l'employée <b>${this.user.prenom} ${this.user.nom}</b>?`,
      nzOnOk: () => {
        this.employeService.deleteOneEmploye(this.matricule).subscribe(
          res => {
          },
          err => {
            console.log(err);
          },
          () => {
            this.message.success(`${this.user.prenom} ${this.user.nom} a été supprimé avec success !`);
            this.router.navigate(['../']);
          }
        );

      }
    });
  }

  refresh() {
    this.route.params.subscribe(
      params => {
        this.matricule = params['matricule'];
        this.profileService.getSelectedEmploye(this.matricule)
          .pipe(untilComponentDestroyed(this))
          .subscribe(
            user => {
              this.user = user;
              this.refreshProfile.changeHistory();
              this.refreshProfile.changeDocument();
            },
            err => {
            },
            () => {
            }
          );
      });
  }

}
