import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileExpFormComponent} from './profile-exp-form.component';

describe('ProfileExpFormComponent', () => {
  let component: ProfileExpFormComponent;
  let fixture: ComponentFixture<ProfileExpFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileExpFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileExpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
