import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ExperienceService} from '@app/services/experience.service';
import {FormService} from '@app/services/form.service';
import {Experience} from '@app/models/Experience';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {NzMessageService} from 'ng-zorro-antd';
import {UserService} from '@app/services/user.service';

@Component({
  selector: 'app-profile-exp-form',
  templateUrl: './profile-exp-form.component.html',
  styleUrls: ['./profile-exp-form.component.sass']
})
export class ProfileExpFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input() add: boolean;
  @Input() edit: boolean;

  @Output() expChanged: EventEmitter<any> = new EventEmitter<any>();
  expFormGroup: FormGroup;
  matricule: number;
  @Input() experience: Experience;

  constructor(private fb: FormBuilder,
              private router: ActivatedRoute,
              public formService: FormService,
              public userService: UserService,
              private message: NzMessageService,
              public experienceService: ExperienceService) {
  }

  ngOnInit() {
    this.matricule = this.router.snapshot.params['matricule'];
    this.createExperienceForm();
  }

  createExperienceForm() {
    this.expFormGroup = this.fb.group(
      {
        mois: ['', Validators.required],
        lieu: ['', Validators.required],
        note: [1, Validators.compose([Validators.required, Validators.max(5), Validators.min(1)])],
        description: ['', Validators.required],
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.edit) {
      this.expFormGroup.setValue({
        mois: this.experience.dureeMois,
        lieu: this.experience.lieu,
        note: this.experience.note,
        description: this.experience.description
      });
    }
  }

  ngOnDestroy(): void {
  }

  onFormSubmit() {
    if (this.add) {
      this.experienceService.addExperience(this.getExperience(), this.matricule)
        .pipe(untilComponentDestroyed(this))
        .subscribe(
          res => {
          },
          err => {
          },
          () => {
            this.message.success('Experience a été ajouté avec success.');
            this.expChanged.emit();
          }
        );
    }
    if (this.edit) {
      this.experienceService.updateExperience(this.getExperience(), this.matricule)
        .pipe(untilComponentDestroyed(this))
        .subscribe(
          res => {
          },
          err => {
          },
          () => {
            this.message.success('Experience a été modifié avec success.');
            this.expChanged.emit();
          }
        );
    }
  }

  getExperience(): Experience {
    return {
      experienceId: this.edit ? this.experience.experienceId : 0,
      description: this.expFormGroup.get('description').value,
      note: this.expFormGroup.get('note').value,
      lieu: this.expFormGroup.get('lieu').value,
      dureeMois: this.expFormGroup.get('mois').value
    };
  }
}
