import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfilePrimeComponent} from './profile-prime.component';

describe('ProfilePrimeComponent', () => {
  let component: ProfilePrimeComponent;
  let fixture: ComponentFixture<ProfilePrimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfilePrimeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePrimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
