import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableColumn} from '@swimlane/ngx-datatable';
import {TableType} from '@app/models/enums/TableType';
import {Prime} from '@app/models/Prime';
import {ActivatedRoute} from '@angular/router';
import {FormService} from '@app/services/form.service';
import {PrimeService} from '@app/services/prime.service';
import {FormActions} from '@app/models/events/form-actions';
import {NzMessageService} from 'ng-zorro-antd';
import {UserService} from '@app/services/user.service';

@Component({
  selector: 'app-profile-prime',
  templateUrl: './profile-prime.component.html',
  styleUrls: ['./profile-prime.component.sass']
})
export class ProfilePrimeComponent implements OnInit, OnDestroy, FormActions {
  showForm = {add: false, edit: false, delete: false};
  @ViewChild('datatable') dataTable;
  prime: Prime;
  selectedPrimes: Prime[] = [];
  rowButtonType;
  query: string;
  tableType = TableType.ProfilePrime;

  columns: TableColumn[] = [
    {prop: 'primeId', name: 'ID', width: 50},
    {prop: 'prime'},
    {prop: 'plafond'},
    {prop: 'condition'},
  ];

  constructor(private route: ActivatedRoute,
              private formService: FormService,
              private message: NzMessageService,
              private userService: UserService,
              private primeService: PrimeService) {
    this.rowButtonType = this.userService.isAdmin() ? RowButtonType.ADD_DELETE_EDIT_REPORT : RowButtonType.NO_BUTTON;
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  onSelect(s): void {
    this.selectedPrimes = s.selected;
    if (s.selected.length > 0) {
      this.prime = s.selected.slice(-1)[0];
    }
  }

  onChange() {
    this.refresh();
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onDelete() {
    if (this.selectedPrimes.length > 0) {
      this.primeService.deletePrime(this.route.snapshot.params['matricule']
        , this.selectedPrimes.map((c: Prime) => c.primeId))
        .subscribe(
          res => {
          }, err => {
            console.log(err);
          }, () => {
            this.message.success('prime(s) ajouté');
            this.showForm.delete = true;
            this.refresh();
          }
        );
    }
  }

  onEdit() {
    if (this.selectedPrimes.length === 0) {
      // hide the form when nothing selected;
      if (this.showForm.edit) {
        this.showForm.edit = false;
        return;
      }
      this.message.info('Aucun prime selectionnée');
    } else {
      this.formService.showEditForm(this.showForm);
    }
  }

  refresh() {
    this.dataTable.onPage({offset: 0});
  }

  onSearch(query: string) {
    this.query = query;
  }

  onReport() {
  }
}
