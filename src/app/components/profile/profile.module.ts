import {NgModule} from '@angular/core';
import {SharedModule} from '@app/components/shared/shared.module';
import {ProfileComponent} from '@app/components/profile/profile/profile.component';
import {ProfileRoutingModule} from '@app/components/profile/profile-routing.module';
import {ProfileInfoComponent} from './profile-info/profile-info.component';
import {ProfileDocumentComponent} from './profile-document/profile-document.component';
import {ProfileLangComponent} from './profile-lang/profile-lang.component';
import {ProfileCompetenceComponent} from './profile-competence/profile-competence.component';
import {ProfileExpComponent} from './profile-exp/profile-exp.component';
import {ProfileContactComponent} from './profile-contact/profile-contact.component';
import {ProfileContactFormComponent} from './profile-contact-form/profile-contact-form.component';
import {ProfileDocumentFormComponent} from './profile-document-form/profile-document-form.component';
import {ProfilePrimeComponent} from './profile-prime/profile-prime.component';
import {ProfilePrimeFormComponent} from './profile-prime-form/profile-prime-form.component';
import {ProfileLangFormComponent} from './profile-lang-form/profile-lang-form.component';
import {ProfileCompetenceFormComponent} from './profile-competence-form/profile-competence-form.component';
import {ProfileExpFormComponent} from './profile-exp-form/profile-exp-form.component';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {ProfileHistoriqueComponent} from '@app/components/profile/profile-historique/profile-historique.component';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileInfoComponent,
    ProfileDocumentComponent,
    ProfileLangComponent,
    ProfileCompetenceComponent,
    ProfileExpComponent,
    ProfileContactComponent,
    ProfileContactFormComponent,
    ProfileDocumentFormComponent,
    ProfilePrimeComponent,
    ProfilePrimeFormComponent,
    ProfileLangFormComponent,
    ProfileHistoriqueComponent,
    ProfileCompetenceFormComponent,
    ProfileExpFormComponent],
  imports: [
    SharedModule,
    ScrollToModule.forRoot(),
    ProfileRoutingModule
  ]
})
export class ProfileModule {
}
