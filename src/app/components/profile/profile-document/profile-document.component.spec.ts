import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileDocumentComponent} from './profile-document.component';

describe('ProfileDocumentComponent', () => {
  let component: ProfileDocumentComponent;
  let fixture: ComponentFixture<ProfileDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileDocumentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
