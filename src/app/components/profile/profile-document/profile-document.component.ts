import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {FormService} from '@app/services/form.service';
import {DocumentService} from '@app/services/document.service';
import {Document} from '@app/models/Document';
import {OnAdd, OnChange, OnDelete, OnSearch, OnSelect} from '@app/models/events/actions';
import {RefreshProfileService} from '@app/services/refresh-profile.service';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-profile-document',
  templateUrl: './profile-document.component.html',
  styleUrls: ['./profile-document.component.sass']
})
export class ProfileDocumentComponent implements OnInit, OnDestroy, OnSelect, OnAdd, OnDelete, OnChange, OnSearch {
  rowButtonType = RowButtonType.ADD_DELETE_REPORT;
  tableType = TableType.ProfileDocument;
  showForm = {add: false, edit: false, delete: false};
  @ViewChild('datatable') dataTable;
  selectedDocuments: Document[] = [];
  query: string;
  columns: TableColumn[] = [
    {prop: 'documentId', name: 'ID', width: 50},
    {prop: 'createdAt', name: 'Date'},
    {prop: 'typeDocument.description', name: 'type'}
  ];

  constructor(private route: ActivatedRoute,
              private documentService: DocumentService,
              private message: NzMessageService,
              private refreshProfile: RefreshProfileService,
              private formService: FormService) {
  }

  ngOnInit() {
    this.refreshProfile.refreshDocument.subscribe(m => {
      this.refresh();
    });
  }

  ngOnDestroy(): void {
  }

  onSelect(s): void {
    this.selectedDocuments = s.selected;
    if (s.selected.length > 0) {
    }
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onDelete() {
    if (this.selectedDocuments.length > 0) {
      this.documentService.deleteDocument(this.selectedDocuments.map((c: Document) => c.documentId))
        .subscribe(
          res => {
          }, err => {
            console.log(err);
          }, () => {
            this.message.success('document(s) selectionnée supprimé');
            this.refreshProfile.changeInfo();
            this.refresh();
          }
        );
    }
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }

  onChange() {
    this.refresh();
  }

  onSearch(query: string) {
    this.query = query;
  }
}
