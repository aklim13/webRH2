import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableColumn} from '@swimlane/ngx-datatable';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {Language} from '@app/models/Language';
import {NzMessageService} from 'ng-zorro-antd';
import {LanguageService} from '@app/services/language.service';

@Component({
  selector: 'app-settings-language',
  templateUrl: './settings-language.component.html',
  styleUrls: ['./settings-language.component.sass']
})
export class SettingsLanguageComponent implements OnInit {
  languageForm: FormGroup;
  buttonType = RowButtonType.DELETE;
  typeTable = TableType.Language;
  buttonText = 'Ajouter';
  @ViewChild('dataTable') dataTable;
  formLayout = [
    {label: 'langue', name: 'language', type: 'text'},
  ];
  columns: TableColumn[] = [
    {prop: 'langueId', name: 'ID', width: 40},
    {prop: 'language', name: 'Langue'},
  ];
  selected: Language[];

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private languageService: LanguageService) {
  }

  ngOnInit() {
    this.languageForm = this.fb.group({
      language: ['', Validators.required],
    });
  }


  onFormSubmit(event) {
    if (this.selected.length === 1) {
      if (this.isSelectedListValid() && this.selected.length === 1) {
        this.languageService.updateLanguages({
          langueId: this.selected[0].langueId,
          language: this.languageForm.get('language').value,
        }).subscribe(
          res => {
            this.message.success('Langue modifié');
            this.refresh();
          },
          err => {
            this.message.error(err);
          }
        );
      } else {
        this.message.error('veuillez selectionner une seule langue à modifier');
      }
    } else if (this.selected.length === 0 || this.selected.length > 1) {
      this.languageService.addLanguages(this.languageForm.get('language').value).subscribe(
        res => {
          this.message.success('Langue ajouté.');
          this.refresh();
        },
        err => {
          this.message.error('Langue existe déjà.');
        }
      );
    }
  }

  onDelete() {
    if (this.isSelectedListValid()) {
      this.languageService.deleteLanguages(this.selected.map(l => l.langueId)).subscribe(
        res => {
          this.message.success('Langue supprimé.');
          this.refresh();
        },
        err => {
          this.message.error(err);
        }
      );
    }
  }

  onSelect(selected) {
    this.selected = selected.selected;
    if (this.selected.length === 0) {
      this.buttonText = 'Ajouter';
      this.languageForm.reset();
    }
    if (this.selected.length > 0) {
      this.buttonText = 'Modifier';
      const v = this.selected[this.selected.length - 1];
      this.languageForm.setValue({
        language: v.language
      });
    }
  }

  isSelectedListValid() {
    if (this.selected.length === 0) {
      this.message.error('veuillez selectionner au moins une seule langue');
      return false;
    }
    return true;
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }
}
