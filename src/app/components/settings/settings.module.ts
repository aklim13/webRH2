import {NgModule} from '@angular/core';
import {SettingsPrimeComponent} from './settings-prime/settings-prime.component';
import {SettingsChangePasswordComponent} from './settings-change-password/settings-change-password.component';
import {SettingsCompetenceComponent} from './settings-competence/settings-competence.component';
import {SettingsPosteComponent} from './settings-poste/settings-poste.component';
import {SettingsLanguageComponent} from './settings-language/settings-language.component';
import {SettingsServiceComponent} from './settings-service/settings-service.component';
import {SettingsHomeComponent} from './settings-home/settings-home.component';
import {SharedModule} from '@app/components/shared/shared.module';
import {SettingsRoutingModule} from '@app/components/settings/settings-routing.module';

@NgModule({
  imports: [
    SharedModule,
    SettingsRoutingModule
  ],
  exports: [
    SharedModule
  ],
  declarations: [
    SettingsPrimeComponent,
    SettingsChangePasswordComponent,
    SettingsCompetenceComponent,
    SettingsPosteComponent,
    SettingsLanguageComponent,
    SettingsServiceComponent,
    SettingsHomeComponent
  ]
})
export class SettingsModule {
}
