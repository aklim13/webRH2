import {NgModule} from '@angular/core';
import {RoleGuard} from '@app/guards/role.guard';
import {AuthGuard} from '@app/guards/auth.guard';
import {RouterModule, Routes} from '@angular/router';
import {SettingsHomeComponent} from '@app/components/settings/settings-home/settings-home.component';

const settingsRoute: Routes = [
  {
    path: 'configuration',
    component: SettingsHomeComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {roles: ['admin']}
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(settingsRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsRoutingModule {
}
