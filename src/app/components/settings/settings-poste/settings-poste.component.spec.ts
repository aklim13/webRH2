import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsPosteComponent} from './settings-poste.component';

describe('SettingsPosteComponent', () => {
  let component: SettingsPosteComponent;
  let fixture: ComponentFixture<SettingsPosteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsPosteComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
