import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableColumn} from '@swimlane/ngx-datatable';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {NzMessageService} from 'ng-zorro-antd';
import {PosteService} from '@app/services/poste.service';
import {Poste} from '@app/models/DataTable';

@Component({
  selector: 'app-settings-poste',
  templateUrl: './settings-poste.component.html',
  styleUrls: ['./settings-poste.component.sass']
})
export class SettingsPosteComponent implements OnInit {
  posteForm: FormGroup;
  buttonType = RowButtonType.DELETE;
  typeTable = TableType.Postes;
  buttonText = 'Ajouter';
  @ViewChild('dataTable') dataTable;
  formLayout = [
    {label: 'Poste', name: 'poste', type: 'text'},
    {label: 'Salaire', name: 'salaire', type: 'number'},
  ];
  columns: TableColumn[] = [
    {prop: 'posteId', name: 'ID', width: 40},
    {prop: 'poste', name: 'Poste'},
    {prop: 'salaire', name: 'Salaire'},
  ];
  selected: Poste[];

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private posteService: PosteService) {
  }

  ngOnInit() {
    this.posteForm = this.fb.group({
      poste: ['', Validators.required],
      salaire: ['', Validators.required],
    });
  }


  onFormSubmit(event) {
    if (this.selected.length === 1) {
      if (this.isSelectedListValid() && this.selected.length === 1) {
        this.posteService.updatePoste({
          posteId: this.selected[0].posteId,
          poste: this.posteForm.get('poste').value,
          salaire: this.posteForm.get('salaire').value
        }).subscribe(
          res => {
            this.message.success('poste modifié');
            this.refresh();
          },
          err => {
            this.message.error(err);
          }
        );
      } else {
        this.message.error('veuillez selectionner un seul poste à modifier');
      }
    } else if (this.selected.length === 0 || this.selected.length > 1) {
      this.posteService.addPoste({
        poste: this.posteForm.get('poste').value,
        salaire: this.posteForm.get('salaire').value
      }).subscribe(
        res => {
          this.message.success('Poste ajouté.');
          this.refresh();
        },
        err => {
          this.message.error('Poste existe déjà.');
        }
      );
    }
  }

  onDelete() {
    if (this.isSelectedListValid()) {
      this.posteService.deletePostes(this.selected.map(p => p.posteId)).subscribe(
        res => {
          this.message.success('Poste supprimé.');
          this.refresh();
        },
        err => {
          this.message.error(err);
        }
      );
    }
  }

  onSelect(selected) {
    this.selected = selected.selected;
    if (this.selected.length === 0) {
      this.buttonText = 'Ajouter';
      this.posteForm.reset();
    }
    if (this.selected.length > 0) {
      this.buttonText = 'Modifier';
      const v = this.selected[this.selected.length - 1];
      this.posteForm.setValue({
        poste: v.poste,
        salaire: v.salaire
      });
    }
  }

  isSelectedListValid() {
    if (this.selected.length === 0) {
      this.message.error('veuillez selectionner au moins un seul poste');
      return false;
    }
    return true;
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }

}
