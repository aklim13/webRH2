import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompetenceService} from '@app/services/competence.service';
import {OnDelete, OnEdit, OnSelect} from '@app/models/events/actions';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {TableColumn} from '@swimlane/ngx-datatable';
import {NzMessageService} from 'ng-zorro-antd';
import {Competence} from '@app/models/Competence';

@Component({
  selector: 'app-settings-competence',
  templateUrl: './settings-competence.component.html',
  styleUrls: ['./settings-competence.component.sass']
})
export class SettingsCompetenceComponent implements OnInit, OnDelete, OnEdit, OnSelect {
  competenceForm: FormGroup;
  buttonType = RowButtonType.DELETE;
  typeTable = TableType.Competences;
  buttonText = 'Ajouter';
  @ViewChild('dataTable') dataTable;
  formLayout = [
    {label: 'Compétence', name: 'competence', type: 'text'}
  ];
  columns: TableColumn[] = [
    {prop: 'comptenceId', name: 'ID', width: 40},
    {prop: 'competence', name: 'Competence'},
  ];
  selected: Competence[];

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private competenceService: CompetenceService) {
  }

  ngOnInit() {
    this.competenceForm = this.fb.group({
      competence: ['', Validators.required]
    });
  }

  onFormSubmit(event) {
    if (this.selected.length === 1) {
      if (this.isSelectedListValid() && this.selected.length === 1) {
        this.competenceService.editCompetence({
          comptenceId: this.selected[0].comptenceId,
          competence: this.competenceForm.get('competence').value
        }).subscribe(
          res => {
            this.message.success('Compétence modifié');
            this.refresh();
          },
          err => {
            this.message.error(err);
          }
        );
      } else {
        this.message.error('veuillez selectionner une seule compétence à modifier');
      }
    } else if (this.selected.length === 0 || this.selected.length > 1) {
      this.competenceService.addCompetence(event.competence).subscribe(
        res => {
          this.message.success('Compétence ajouté.');
          this.refresh();
        },
        err => {
          this.message.error('Compétence existe déjà.');
        }
      );
    }
  }

  onDelete() {
    if (this.isSelectedListValid()) {
      this.competenceService.deleteCompetences(this.selected.map(c => c.comptenceId)).subscribe(
        res => {
          this.message.success('Compétence supprimé.');
          this.refresh();
        },
        err => {
          this.message.error(err);
        }
      );
    }
  }

  onEdit() {

  }

  onSelect(selected) {
    this.selected = selected.selected;
    if (this.selected.length === 0) {
      this.buttonText = 'Ajouter';
      this.competenceForm.get('competence').setValue('');
    }
    if (this.selected.length > 0) {
      this.buttonText = 'Modifier';
      this.competenceForm.get('competence').setValue(this.selected[this.selected.length - 1].competence);
    }
  }

  isSelectedListValid() {
    if (this.selected.length === 0) {
      this.message.error('veuillez selectionner au moins une seule compétence');
      return false;
    }
    return true;
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }

}
