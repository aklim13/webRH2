import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsCompetenceComponent} from './settings-competence.component';

describe('SettingsCompetenceComponent', () => {
  let component: SettingsCompetenceComponent;
  let fixture: ComponentFixture<SettingsCompetenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsCompetenceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsCompetenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
