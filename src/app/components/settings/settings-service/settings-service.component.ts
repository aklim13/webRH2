import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableColumn} from '@swimlane/ngx-datatable';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableType} from '@app/models/enums/TableType';
import {NzMessageService} from 'ng-zorro-antd';
import {EmployeService} from '@app/services/employe.service';
import {Service} from '@app/models/DataTable';

@Component({
  selector: 'app-settings-service',
  templateUrl: './settings-service.component.html',
  styleUrls: ['./settings-service.component.sass']
})
export class SettingsServiceComponent implements OnInit {
  serviceForm: FormGroup;
  buttonType = RowButtonType.DELETE;
  typeTable = TableType.Services;
  buttonText = 'Ajouter';
  @ViewChild('dataTable') dataTable;
  formLayout = [
    {label: 'Service', name: 'service', type: 'text'},
  ];
  columns: TableColumn[] = [
    {prop: 'serviceId', name: 'ID', width: 40},
    {prop: 'service', name: 'Service'},
  ];
  selected: Service[];

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private employeService: EmployeService) {
  }

  ngOnInit() {
    this.serviceForm = this.fb.group({
      service: ['', Validators.required],
    });
  }


  onFormSubmit(event) {
    if (this.selected.length === 1) {
      if (this.isSelectedListValid() && this.selected.length === 1) {
        alert(this.serviceForm.get('service').value);
        this.employeService.updateService({
          serviceId: this.selected[0].serviceId,
          service: this.serviceForm.get('service').value,
        }).subscribe(
          res => {
            this.message.success('service modifié');
            this.refresh();
          },
          err => {
            this.message.error(err);
          }
        );
      } else {
        this.message.error('veuillez selectionner un seul service à modifier');
      }
    } else if (this.selected.length === 0 || this.selected.length > 1) {
      this.employeService.addService(this.serviceForm.get('service').value).subscribe(
        res => {
          this.message.success('Service ajouté.');
          this.refresh();
        },
        err => {
          this.message.error('Service existe déjà.');
        }
      );
    }
  }

  onDelete() {
    if (this.isSelectedListValid()) {
      this.employeService.deleteServices(this.selected.map(s => s.serviceId)).subscribe(
        res => {
          this.message.success('Service supprimé.');
          this.refresh();
        },
        err => {
          this.message.error(err);
        }
      );
    }
  }

  onSelect(selected) {
    this.selected = selected.selected;
    if (this.selected.length === 0) {
      this.buttonText = 'Ajouter';
      this.serviceForm.reset();
    }
    if (this.selected.length > 0) {
      this.buttonText = 'Modifier';
      const v = this.selected[this.selected.length - 1];
      this.serviceForm.setValue({
        service: v.service
      });
    }
  }

  isSelectedListValid() {
    if (this.selected.length === 0) {
      this.message.error('veuillez selectionner au moins un seul service');
      return false;
    }
    return true;
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }
}
