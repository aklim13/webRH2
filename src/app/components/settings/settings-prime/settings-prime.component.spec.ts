import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsPrimeComponent} from './settings-prime.component';

describe('SettingsPrimeComponent', () => {
  let component: SettingsPrimeComponent;
  let fixture: ComponentFixture<SettingsPrimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsPrimeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPrimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
