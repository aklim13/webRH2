import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableColumn} from '@swimlane/ngx-datatable';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {NzMessageService} from 'ng-zorro-antd';
import {TableType} from '@app/models/enums/TableType';
import {Prime} from '@app/models/Prime';
import {PrimeService} from '@app/services/prime.service';

@Component({
  selector: 'app-settings-prime',
  templateUrl: './settings-prime.component.html',
  styleUrls: ['./settings-prime.component.sass']
})
export class SettingsPrimeComponent implements OnInit {
  primeForm: FormGroup;
  buttonType = RowButtonType.DELETE;
  typeTable = TableType.Prime;
  buttonText = 'Ajouter';
  @ViewChild('dataTable') dataTable;
  formLayout = [
    {label: 'Prime', name: 'prime', type: 'text'},
    {label: 'Plafond', name: 'plafond', type: 'number'},
    {label: 'Condition', name: 'condition', type: 'text'},
  ];
  columns: TableColumn[] = [
    {prop: 'primeId', name: 'ID', width: 40},
    {prop: 'prime', name: 'Prime'},
    {prop: 'plafond', name: 'plafond'},
    {prop: 'condition', name: 'Condition'},
  ];
  selected: Prime[];

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private primeService: PrimeService) {
  }

  ngOnInit() {
    this.primeForm = this.fb.group({
      prime: ['', Validators.required],
      plafond: ['', Validators.required],
      condition: ['', Validators.required]
    });
  }


  onFormSubmit(event) {
    if (this.selected.length === 1) {
      if (this.isSelectedListValid() && this.selected.length === 1) {
        this.primeService.modifyPrime({
          primeId: this.selected[0].primeId,
          prime: this.primeForm.get('prime').value,
          condition: this.primeForm.get('condition').value,
          plafond: this.primeForm.get('plafond').value,
        }).subscribe(
          res => {
            this.message.success('Prime modifié');
            this.refresh();
          },
          err => {
            this.message.error(err);
          }
        );
      } else {
        this.message.error('veuillez selectionner une seule prime à modifier');
      }
    } else if (this.selected.length === 0 || this.selected.length > 1) {
      this.primeService.addPrime({
        prime: this.primeForm.get('prime').value,
        condition: this.primeForm.get('condition').value,
        plafond: this.primeForm.get('plafond').value,
      }).subscribe(
        res => {
          this.message.success('Prime ajouté.');
          this.refresh();
        },
        err => {
          this.message.error('Prime existe déjà.');
        }
      );
    }
  }

  onDelete() {
    if (this.isSelectedListValid()) {
      this.primeService.deletePrimes(this.selected.map(p => p.primeId)).subscribe(
        res => {
          this.message.success('Prime supprimé.');
          this.refresh();
        },
        err => {
          this.message.error(err);
        }
      );
    }
  }

  onEdit() {

  }

  onSelect(selected) {
    this.selected = selected.selected;
    if (this.selected.length === 0) {
      this.buttonText = 'Ajouter';
      this.primeForm.reset();
    }
    if (this.selected.length > 0) {
      this.buttonText = 'Modifier';
      const p = this.selected[this.selected.length - 1];
      this.primeForm.setValue({
        prime: p.prime,
        condition: p.condition,
        plafond: p.plafond
      });
    }
  }

  isSelectedListValid() {
    if (this.selected.length === 0) {
      this.message.error('veuillez selectionner au moins une seule prime');
      return false;
    }
    return true;
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }
}
