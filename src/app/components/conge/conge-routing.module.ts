import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@app/guards/auth.guard';
import {RoleGuard} from '@app/guards/role.guard';
import {CongeDemandesComponent} from '@app/components/conge/conge-demandes/conge-demandes.component';
import {CongeListComponent} from '@app/components/conge/conge-list/conge-list.component';
import {CongeListEmpComponent} from '@app/components/conge/conge-list-emp/conge-list-emp.component';
import {CongeStatsComponent} from '@app/components/conge/conge-stats/conge-stats.component';
import {EmpCardComponent} from '@app/components/shared/emp-card/emp-card.component';

const congeRoutes: Routes = [
  {path: 'test', component: EmpCardComponent},
  {
    path: 'conge/demandes',
    component: CongeDemandesComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin']}
  },
  {
    path: 'conge/statistique',
    component: CongeStatsComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin', 'employe']}
  },
  {
    path: 'conge/historique',
    component: CongeListComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin']}
  },
  {
    path: 'conge/demandes/me',
    component: CongeListEmpComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['employe']}
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(congeRoutes)
  ],
  exports: [RouterModule]
})
export class CongeRoutingModule {
}
