import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CongeStatsComponent} from './conge-stats.component';

describe('CongeStatsComponent', () => {
  let component: CongeStatsComponent;
  let fixture: ComponentFixture<CongeStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CongeStatsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongeStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
