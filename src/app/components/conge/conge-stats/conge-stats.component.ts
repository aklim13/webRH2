import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '@app/services/user.service';
import {StatsService} from '@app/services/stats.service';
import {BaseChartComponent} from '@swimlane/ngx-charts';

@Component({
  selector: 'app-conge-stats',
  templateUrl: './conge-stats.component.html',
  styleUrls: ['./conge-stats.component.sass']
})
export class CongeStatsComponent implements OnInit {
  isData = false;
  congeStats: CongeStats;
  @ViewChild(BaseChartComponent) private chart;

  constructor(private userService: UserService,
              private statsService: StatsService) {

  }

  ngOnInit() {
    this.getCongeByDays(7);
  }

  onCongeDays(days: number) {
    this.getCongeByDays(days);
  }

  refreshChart() {
    if (this.chart !== undefined) {
      this.chart.ngOnDestroy();
      this.chart.chart = this.chart.getChartBuilder(this.chart.ctx);
    }
  }

  getCongeByDays(days: number) {
    if (this.userService.isAdmin()) {
      this.statsService.getCongeStatsByDays(days).subscribe(
        stats => {
          this.congeStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    } else {
      this.statsService.getCurrentUserCongeStatsByDays(days).subscribe(
        stats => {
          this.congeStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    }
  }

  onCongeDate(res: any) {
    if (this.userService.isAdmin()) {
      this.statsService.getCongeStatsByDate(res.beginDate, res.endDate).subscribe(
        stats => {
          this.congeStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    } else {
      this.statsService.getCurrentUserCongeStatsByDate(res.beginDate, res.endDate).subscribe(
        stats => {
          this.congeStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    }
  }
}
