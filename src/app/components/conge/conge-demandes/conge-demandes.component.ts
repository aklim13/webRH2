import {Component, OnInit, ViewChild} from '@angular/core';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableColumn} from '@swimlane/ngx-datatable';
import {CongeService} from '@app/services/conge.service';
import {TableType} from '@app/models/enums/TableType';
import {NzMessageService} from 'ng-zorro-antd';
import {CongeDemande} from '@app/models/lists/CongeList';

@Component({
  selector: 'app-conge-demandes',
  templateUrl: './conge-demandes.component.html',
  styleUrls: ['./conge-demandes.component.sass']
})
export class CongeDemandesComponent implements OnInit {
  tableType = TableType.CongeDemandes;
  rowButtonType = RowButtonType.ACCEPT_REFUSE;
  selectedDemandes: CongeDemande[] = [];
  query: string;
  @ViewChild('datatable') dataTable;
  columns: TableColumn[] = [
    {prop: 'congeId', name: 'ID', width: 50},
    {prop: 'fullName', name: 'Nom complet'},
    {prop: 'dateDebut', name: 'Date début'},
    {prop: 'dateFin', name: 'Date fin'},
    {prop: 'raison', name: 'Raison'},
    {prop: 'sum', name: 'Nombre de jour'}
  ];

  constructor(private congeService: CongeService,
              private message: NzMessageService) {
  }

  ngOnInit() {
  }

  onSelect(s): void {
    this.selectedDemandes = s.selected;
    if (s.selected.length > 0) {

    }
  }

  onSearch(query: string) {
    this.query = query;
  }

  onChange() {
    this.refresh();
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }

  onAccept() {
    this.answerConge(this.selectedDemandes.map(conge => conge.congeId), 'ACCEPTED');
    this.message.info('Vous avez accepter tous les congés.');
  }

  onRefuse() {
    this.answerConge(this.selectedDemandes.map(conge => conge.congeId), 'REFUSED');
    this.message.info('Vous avez refusé tous les congés.');
  }

  onCheck() {
    this.answerConge(this.selectedDemandes.map(conge => conge.congeId), 'CHECKING');
    this.message.info('Congé en mode de vérification');
  }

  answerConge(congeId: number[], answer: string) {
    this.congeService.answerConge(congeId, answer).subscribe(
      res => {
        this.refresh();
      }
    );
  }
}
