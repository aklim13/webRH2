import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CongeDemandesComponent} from './conge-demandes.component';

describe('CongeDemandesComponent', () => {
  let component: CongeDemandesComponent;
  let fixture: ComponentFixture<CongeDemandesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CongeDemandesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongeDemandesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
