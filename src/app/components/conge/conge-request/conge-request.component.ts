import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CongeService} from '@app/services/conge.service';
import {TypeConge} from '@app/models/TypeConge';
import {DatePipe} from '@angular/common';
import {CongeDemander} from '@app/models/lists/CongeList';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-conge-request',
  templateUrl: './conge-request.component.html',
  styleUrls: ['./conge-request.component.sass']
})
export class CongeRequestComponent implements OnInit {
  uploadedFiles = [];
  reset = 1;
  congeForm: FormGroup;
  typeConges: TypeConge[];
  conge: CongeDemander;
  beginDate: string;
  endDate: string;
  @Input() add: Boolean;
  @Input() delete: Boolean;
  @Output() congeChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private congeService: CongeService) {
  }

  ngOnInit() {
    this.congeService.getTypeConges().subscribe(
      res => {
        this.typeConges = res;
      }
    );
    this.congeForm = this.fb.group({
      typeConge: [, Validators.required],
      dateRange: [, Validators.required],
      raison: ['', Validators.required]
    });
  }

  onFormSubmit() {
    this.conge = {
      dateDebut: this.beginDate,
      dateFin: this.endDate,
      raison: this.congeForm.get('raison').value,
      typeCongeId: this.congeForm.get('typeConge').value.typeCongeId
    };
    const file = this.uploadedFiles[0] ? this.uploadedFiles[0] : null;
    this.congeService.demandeConge(this.conge).subscribe(
      res => {
        this.message.info('Demande de congé a été envoyé avec success.');
        this.congeChanged.emit();
      }
    );
  }

  onFileContent(event: any) {
    if (event.reset) {
      this.onResetClicked();
    } else {
      this.uploadedFiles.push(event);
    }
  }

  onResetClicked() {
    this.reset++;
    this.congeForm.reset();
    this.uploadedFiles = [];
  }

  onDateRangeChange(result) {
    if (result) {
      const datePipe = new DatePipe('en_us');
      this.beginDate = datePipe.transform(result[0], 'yyyy-MM-dd');
      this.endDate = datePipe.transform(result[1], 'yyyy-MM-dd');
    }
  }
}
