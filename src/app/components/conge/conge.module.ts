import {NgModule} from '@angular/core';
import {SharedModule} from '@app/components/shared/shared.module';
import {CongeRoutingModule} from '@app/components/conge/conge-routing.module';
import {CongeDemandesComponent} from './conge-demandes/conge-demandes.component';
import {CongeListComponent} from './conge-list/conge-list.component';
import {CongeRequestComponent} from './conge-request/conge-request.component';
import {CongeListEmpComponent} from './conge-list-emp/conge-list-emp.component';
import {CongeStatsComponent} from './conge-stats/conge-stats.component';

@NgModule({
  imports: [
    SharedModule,
    CongeRoutingModule
  ],
  exports: [
    SharedModule
  ],
  declarations: [
    CongeDemandesComponent,
    CongeListComponent,
    CongeRequestComponent,
    CongeListEmpComponent,
    CongeStatsComponent]
})
export class CongeModule {
}
