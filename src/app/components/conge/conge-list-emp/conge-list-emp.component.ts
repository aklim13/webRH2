import {Component, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {TableType} from '@app/models/enums/TableType';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {CongeDemande} from '@app/models/lists/CongeList';
import {CongeService} from '@app/services/conge.service';
import {NzMessageService} from 'ng-zorro-antd';
import {FormService} from '@app/services/form.service';
import {UserService} from '@app/services/user.service';

@Component({
  selector: 'app-conge-list-emp',
  templateUrl: './conge-list-emp.component.html',
  styleUrls: ['./conge-list-emp.component.sass']
})
export class CongeListEmpComponent implements OnInit {
  tableType = TableType.CongeMesDemandes;
  showForm = {add: false, delete: false};
  rowButtonType = RowButtonType.DEMANDER_CONGE;
  query: string;
  selectedConges: CongeDemande[] = [];
  @ViewChild('datatable') dataTable;
  columns: TableColumn[] = [
    {prop: 'congeId', name: 'ID', width: 50},
    {prop: 'dateDebut', name: 'Date début'},
    {prop: 'dateFin', name: 'Date fin'},
    {prop: 'raison', name: 'Raison'},
    {prop: 'sum', name: 'Nombre de jour'}
  ];

  constructor(private congeService: CongeService,
              private formService: FormService,
              private userService: UserService,
              private message: NzMessageService) {
  }

  ngOnInit() {
  }

  onSearch(query: string) {
    this.query = query;
  }

  onChange() {
    this.refresh();
  }

  onCancel() {
    if (this.selectedConges.length === 1) {
      this.congeService.cancelConge(this.selectedConges[0].congeId, this.userService.getCurrentUserId()).subscribe(
        res => {
          this.message.info('Conge anullé.');
          this.refresh();
        },
        err => {
          this.message.error(err.error.message);
        }
      );
    } else {
      this.message.error('Selectionner un seul congé à anuller.');
    }
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }

  onSelect(s): void {
    this.selectedConges = s.selected;
    if (s.selected.length > 0) {
    }
  }

  onAdd() {
    this.formService.showAddForm(this.showForm);
  }

  onContactChange() {
    this.refresh();
  }

}
