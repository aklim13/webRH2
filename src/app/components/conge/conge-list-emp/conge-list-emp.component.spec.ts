import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CongeListEmpComponent} from './conge-list-emp.component';

describe('CongeListEmpComponent', () => {
  let component: CongeListEmpComponent;
  let fixture: ComponentFixture<CongeListEmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CongeListEmpComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongeListEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
