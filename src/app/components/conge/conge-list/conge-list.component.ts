import {Component, OnInit, ViewChild} from '@angular/core';
import {TableType} from '@app/models/enums/TableType';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {TableColumn} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-conge-list',
  templateUrl: './conge-list.component.html',
  styleUrls: ['./conge-list.component.sass']
})
export class CongeListComponent implements OnInit {
  tableType = TableType.CongeHistorique;
  rowButtonType = RowButtonType.NO_BUTTON;
  query: string;
  @ViewChild('datatable') dataTable;
  columns: TableColumn[] = [
    {prop: 'congeId', name: 'ID', width: 50},
    {prop: 'fullName', name: 'Nom complet'},
    {prop: 'dateDebut', name: 'Date début'},
    {prop: 'dateFin', name: 'Date fin'},
    {prop: 'raison', name: 'Raison'},
    {prop: 'sum', name: 'Nombre de jour'}
  ];

  constructor() {
  }

  ngOnInit() {
  }

  onSearch(query: string) {
    this.query = query;
  }

  onChange() {
    this.refresh();
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }
}
