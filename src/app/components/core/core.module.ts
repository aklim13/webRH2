import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoggerService} from '@app/services/logger.service';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from '@app/services/auth.service';
import {UserService} from '@app/services/user.service';
import {TokenService} from '@app/services/token.service';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {SharedModule} from '@app/components/shared/shared.module';
import {CompetenceService} from '@app/services/competence.service';
import {ExperienceService} from '@app/services/experience.service';
import {DocumentService} from '@app/services/document.service';
import {LanguageService} from '@app/services/language.service';
import {PrimeService} from '@app/services/prime.service';
import {ContactService} from '@app/services/contact.service';
import {ProfileService} from '@app/services/profile.service';
import {EmployeService} from '@app/services/employe.service';
import {StatsService} from '@app/services/stats.service';
import {AbsenceService} from '@app/services/absence.service';
import {CongeService} from '@app/services/conge.service';
import {PosteService} from '@app/services/poste.service';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule
  ],
  providers: [
    LoggerService,
    TokenService,
    AuthService,
    UserService,
    ContactService,
    PrimeService,
    ExperienceService,
    CompetenceService,
    LanguageService,
    StatsService,
    DocumentService,
    CongeService,
    AbsenceService,
    PosteService,
    ProfileService,
    EmployeService
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
