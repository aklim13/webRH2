import {Component, OnInit} from '@angular/core';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PasswordService} from '@app/services/password.service';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.sass']
})
export class PasswordChangeComponent implements OnInit {
  passwordChangeGroup: FormGroup;
  formLayout = [
    {label: 'Ancien mot de passe', name: 'oldPass'},
    {label: 'Nouveau mot de passe', name: 'newPass'},
    {label: 'Confirmer mot de passe', name: 'confirmPass'}
  ];

  buttonType = RowButtonType.NO_BUTTON;

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private passwordService: PasswordService) {
  }

  ngOnInit() {
    this.passwordChangeGroup = this.fb.group({
      oldPass: ['', Validators.required],
      newPass: ['', Validators.required],
      confirmPass: ['', Validators.required]
    });

  }

  onSubmit() {
    const form = this.passwordChangeGroup.value;
    this.passwordService.changePassword({
      confirmPassword: form.confirmPass,
      newPassword: form.newPass,
      oldPassword: form.oldPass,
    }).subscribe(
      res => {
        this.message.success('Mot de passe a été changé avec success.');
      },
      err => {
        this.message.error(err.error.message);
      }
    );
  }

  isPasswordMatching() {
    return this.passwordChangeGroup.get('newPass').value === this.passwordChangeGroup.get('confirmPass').value;
  }

}
