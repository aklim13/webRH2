import {Component, OnInit} from '@angular/core';
import {PasswordService} from '@app/services/password.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {NzMessageService} from 'ng-zorro-antd';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '@app/services/user.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.sass']
})
export class PasswordResetComponent implements OnInit {
  changePasswordForm: FormGroup;
  buttonType = RowButtonType.NO_BUTTON;

  constructor(private passwordService: PasswordService,
              private message: NzMessageService,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private fb: FormBuilder) {
  }

  isPasswordMatching() {
    return this.changePasswordForm.get('password').value === this.changePasswordForm.get('cPassword').value;
  }


  ngOnInit() {
    if (this.userService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
    this.changePasswordForm = this.fb.group({
        password: ['', Validators.required],
        cPassword: ['', Validators.required]
      }
    );
  }

  onSubmit() {
    if (this.isPasswordMatching()) {
      this.passwordService.resetPassword(this.changePasswordForm.get('password').value, this.route.snapshot.params['token']).subscribe(
        res => {
          this.message.success(res.message);
        }, error => {
          this.message.error(error.error.message);
        }
      );
    } else {
      this.message.error('Mot de passe n\'est pas equivalent au mot de passe de confirmation');
    }
  }
}
