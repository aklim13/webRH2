import {NgModule} from '@angular/core';
import {LogoutComponent} from './logout/logout.component';
import {LoginComponent} from './login/login.component';
import {AuthRoutingModule} from './auth-routing.module';
import {SharedModule} from '@app/components/shared/shared.module';
import {CommonModule} from '@angular/common';
import {PasswordForgotComponent} from './password-forgot/password-forgot.component';
import {PasswordResetComponent} from '@app/components/auth/password-reset/password-reset.component';
import {PasswordChangeComponent} from './password-change/password-change.component';

@NgModule({
  declarations: [
    LoginComponent,
    LogoutComponent,
    PasswordResetComponent,
    PasswordForgotComponent,
    PasswordChangeComponent
  ],
  imports: [
    SharedModule,
    AuthRoutingModule
  ],
  exports: [
    CommonModule,
    SharedModule
  ]
})
export class AuthModule {
}
