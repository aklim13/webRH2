import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '@app/services/auth.service';
import {UserService} from '@app/services/user.service';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  model: any = {};

  constructor(private authService: AuthService,
              private router: Router,
              private message: NzMessageService,
              private userService: UserService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    // if user already logged in then don't show login page.
    if (this.userService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  login() {
    this.authService.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          this.router.navigate(['/']);
        },
        error => {
          this.message.error('Veuillez verifier votre cin ou mot de passe');
        },
        () => {
        });
  }
}
