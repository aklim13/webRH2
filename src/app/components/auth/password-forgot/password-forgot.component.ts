import {Component, OnInit} from '@angular/core';
import {PasswordService} from '@app/services/password.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzMessageService} from 'ng-zorro-antd';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {UserService} from '@app/services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-password-forgot',
  templateUrl: './password-forgot.component.html',
  styleUrls: ['./password-forgot.component.sass']
})
export class PasswordForgotComponent implements OnInit {
  forgotForm: FormGroup;
  buttonType = RowButtonType.NO_BUTTON;

  constructor(private passwordService: PasswordService,
              private message: NzMessageService,
              private userService: UserService,
              private router: Router,
              private fb: FormBuilder) {
  }


  ngOnInit() {
    if (this.userService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
    this.forgotForm = this.fb.group({
        cin: ['', Validators.required]
      }
    );
  }

  onSubmit() {
    this.passwordService.requestNewPassword(this.forgotForm.get('cin').value).subscribe(
      res => {
        this.message.success('Veuillez vérifier votre email.');
      },
      err => {
        this.message.error('Cin non trouvé. contacter administration.');
      }
    );
  }
}
