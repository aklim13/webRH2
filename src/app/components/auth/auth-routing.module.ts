import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LogoutComponent} from './logout/logout.component';
import {LoginComponent} from './login/login.component';
import {ErrorNotFoundComponent} from '@app/components/shared/error-not-found/error-not-found.component';
import {PasswordForgotComponent} from '@app/components/auth/password-forgot/password-forgot.component';
import {PasswordResetComponent} from '@app/components/auth/password-reset/password-reset.component';
import {PasswordChangeComponent} from '@app/components/auth/password-change/password-change.component';
import {RoleGuard} from '@app/guards/role.guard';
import {AuthGuard} from '@app/guards/auth.guard';

const loginRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'forgot/password', component: PasswordForgotComponent},
  {path: 'reset/password/:token', component: PasswordResetComponent},
  {
    path: 'change/password',
    component: PasswordChangeComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {roles: ['admin', 'employe']},
  },
  {path: 'not-found', component: ErrorNotFoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(loginRoutes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
