import {Component, OnInit, ViewChild} from '@angular/core';
import {StatsService} from '@app/services/stats.service';
import {Statistique} from '@app/models/Stats';
import {BaseChartComponent} from '@swimlane/ngx-charts';
import {transition, trigger, useAnimation} from '@angular/animations';
import {fadeInLeft, fadeInRight, zoomIn} from 'ng-animate';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  animations: [
    trigger('zoomIn', [transition('* => *', useAnimation(zoomIn))]),
    trigger('fadeInLeft', [transition('* => *', useAnimation(fadeInLeft))]),
    trigger('fadeInRight', [transition('* => *', useAnimation(fadeInRight))])
  ],
  styleUrls: ['./admin-dashboard.component.sass']
})
export class AdminDashboardComponent implements OnInit {
  stats: Statistique;
  isData = false;
  zoomIn: any;
  fadeInLeft: any;
  fadeInRight: any;
  @ViewChild(BaseChartComponent) private chart;

  constructor(private statsService: StatsService) {
  }

  ngOnInit() {
    this.statsService.getEmployeesStats().subscribe(
      stats => {
        this.stats = stats;
        this.isData = true;
      }, err => {
      }
    );

  }

}
