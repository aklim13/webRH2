import {Component, OnInit} from '@angular/core';
import {SingleStats} from '@app/models/Stats';
import {StatsService} from '@app/services/stats.service';

@Component({
  selector: 'app-employe-dashboard',
  templateUrl: './employe-dashboard.component.html',
  styleUrls: ['./employe-dashboard.component.sass']
})
export class EmployeDashboardComponent implements OnInit {
  stats: SingleStats;
  isData = false;

  constructor(private statsService: StatsService) {
  }

  ngOnInit() {
    this.statsService.getSingleEmployeStats().subscribe(
      res => {
        this.stats = res;
        this.isData = true;
      }
    );
  }

}
