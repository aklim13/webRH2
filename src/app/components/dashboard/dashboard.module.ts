import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/components/shared/shared.module';
import {DashboardRoutingModule} from '@app/components/dashboard/dashboard-routing.module';
import {AdminDashboardComponent} from '@app/components/dashboard/admin-dashboard/admin-dashboard.component';
import {HomeComponent} from './home/home.component';
import {EmployeDashboardComponent} from './employe-dashboard/employe-dashboard.component';

@NgModule({
  declarations: [
    AdminDashboardComponent,
    HomeComponent,
    EmployeDashboardComponent
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ],
  exports: [
    CommonModule,
    SharedModule
  ]
})
export class DashboardModule {
}
