import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {UserService} from '@app/services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  isCollapsed = false;
  triggerTemplate = null;
  @ViewChild('trigger') customTrigger: TemplateRef<void>;

  constructor(public userService: UserService,
              private router: Router) {

  }

  ngOnInit(): void {

  }

  changeTrigger(): void {
    this.triggerTemplate = this.customTrigger;
  }

  checkProfileRoute() {
    return this.router.url.includes('profile') && this.userService.getCurrentUserRole() !== 'employe';
  }
}
