import {NgModule} from '@angular/core';
import {AuthGuard} from '@app/guards/auth.guard';
import {RoleGuard} from '@app/guards/role.guard';
import {EmpListComponent} from './emp-list/emp-list.component';
import {RouterModule, Routes} from '@angular/router';
import {EmpForm2Component} from '@app/components/shared/emp-form-2/emp-form-2.component';


const empRoutes: Routes = [
  {
    path: 'employees',
    component: EmpListComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin']},
    children: [
      {path: 'create', component: EmpForm2Component, data: {roles: ['admin']}},
      {path: ':matricule/edit', component: EmpForm2Component, data: {roles: ['admin']}}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(empRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeesRoutingModule {
}
