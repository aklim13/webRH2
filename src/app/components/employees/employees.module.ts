import {NgModule} from '@angular/core';
import {EmpListComponent} from './emp-list/emp-list.component';
import {EmployeesRoutingModule} from './employees-routing.module';
import {SharedModule} from '../shared/shared.module';
import {EmpGridComponent} from './emp-grid/emp-grid.component';

@NgModule({
  declarations: [
    EmpGridComponent,
    EmpListComponent,
  ],
  imports: [
    SharedModule,
    EmployeesRoutingModule],
})
export class EmployeesModule {
}
