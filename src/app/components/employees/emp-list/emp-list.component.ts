import {Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EmployeService} from '@app/services/employe.service';
import 'rxjs/add/operator/takeUntil';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {TableColumn} from '@swimlane/ngx-datatable';
import {EmployePreviewData} from '@app/models/EmployePreviewData';
import {ActivatedRoute, Router} from '@angular/router';
import {EmpRefreshService} from '@app/services/emp-refresh.service';
import {TableType} from '@app/models/enums/TableType';
import {DatatablePageableComponent} from '@app/components/shared/datatable-pageable/datatable-pageable.component';
import {OnAdd, OnDelete, OnEdit, OnMode, OnReport, OnSearch, OnSelect} from '@app/models/events/actions';
import {NzMessageService} from 'ng-zorro-antd';
import {RowButtonType} from '@app/models/enums/RowButtonType';

@Component({
  selector: 'app-emp-list',
  templateUrl: './emp-list.component.html',
  styleUrls: ['./emp-list.component.sass']
})
export class EmpListComponent implements OnInit, OnDestroy, OnAdd, OnDelete, OnEdit, OnMode, OnSelect, OnReport, OnSearch {
  @ViewChild(DatatablePageableComponent) table;
  employees: EmployePreviewData[];
  public innerWidth: any;
  columns: TableColumn[] = [
    {prop: 'matricule', width: 40},
    {prop: 'nom'},
    {prop: 'prenom'},
    {prop: 'cin'},
    {prop: 'poste'},
    {prop: 'note', name: 'Note'},
  ];
  typeTable = TableType.Employe;
  buttonType = RowButtonType.ADD_EDIT_DELETE_MODE;
  switchMode = false;
  selected = [];
  query: string;

  constructor(public empService: EmployeService,
              private router: Router,
              private route: ActivatedRoute,
              private message: NzMessageService,
              private refreshService: EmpRefreshService) {
  }

  ngOnInit() {
    this.refreshService.currentMessage.pipe(untilComponentDestroyed(this)).subscribe(
      m => {
        try {
          this.table.onPage({offset: 0});
        } catch (e) {
        }
      },
    );
    this.innerWidth = window.innerWidth;
  }

  ngOnDestroy() {
  }

  onGridSelect(selected: EmployePreviewData[]) {
    this.selected = selected;
  }

  onSelect({selected}) {
    if (selected.length > 0) {
      console.log('Select Event', selected);
    }
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onDelete() {
    // delete only executed if something is selected.
    if (this.selected.length > 0) {
      this.empService.deleteEmploye(this.selected.map(v => v.matricule)).subscribe(() => {
        this.table.onPage({offset: 0});
      });
    }
  }

  onMode() {
    this.switchMode = !this.switchMode;
  }

  onAdd() {
    if (this.router.url.includes('create')) {
      this.router.navigate(['/employees']);
    } else {
      this.router.navigate(['create'], {relativeTo: this.route});
    }
  }

  onSearch(query: string) {
    this.query = query;
  }

  onEdit() {
    if (this.router.url.includes('edit')) {
      this.router.navigate(['/employees']);
      return;
    } else if (this.selected.length === 0) {
      this.message.warning('Aucun employée n\'est selectionnée');
    } else if (this.selected.length === 1) {
      this.router.navigate([this.selected[0].matricule, 'edit'], {relativeTo: this.route});
    } else {
      this.message.info('pour modifier veuillez selectionner un seul employée');
    }
  }

  onReport() {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth <= 639) {
      this.switchMode = true;
    }
  }
}
