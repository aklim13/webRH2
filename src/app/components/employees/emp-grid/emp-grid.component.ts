import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {EmployeService} from '@app/services/employe.service';
import {EmployePreviewData} from '@app/models/EmployePreviewData';
import {Router} from '@angular/router';
import {zoomIn} from 'ng-animate';
import {transition, trigger, useAnimation} from '@angular/animations';

@Component({
  selector: 'app-emp-grid',
  templateUrl: './emp-grid.component.html',
  animations: [trigger('zoomIn', [transition('* => *', useAnimation(zoomIn))])],
  styleUrls: ['./emp-grid.component.sass']
})
export class EmpGridComponent implements OnInit {
  employeList: EmployePreviewData[] = [];
  selected: EmployePreviewData[] = [];
  @Output() selectEvent = new EventEmitter<EmployePreviewData[]>();
  zoomIn: any;

  constructor(private employeService: EmployeService,
              private router: Router) {
  }

  ngOnInit() {
    this.employeService.getAllEmployees('').subscribe(
      res => {
        this.employeList = res;
      }
    );
  }

  onCardClicked(employe: EmployePreviewData) {
    if (this.selected.includes(employe)) {
      this.selected.splice(this.selected.indexOf(employe), 1);
    } else {
      this.selected.push(employe);
    }
    this.selectEvent.emit(this.selected);
  }

  onProfile(employe: EmployePreviewData) {
    this.router.navigate(['profile', employe.matricule]);
  }
}
