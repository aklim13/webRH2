import {Component, Input, OnInit} from '@angular/core';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {StatsService} from '@app/services/stats.service';

@Component({
  selector: 'app-chart-line',
  templateUrl: './chart-line.component.html',
  styleUrls: ['./chart-line.component.sass']
})
export class ChartLineComponent implements OnInit {
  noRowButton = RowButtonType.NO_BUTTON;
  @Input() title: string;
  @Input() data: Array<any>;
  @Input() labels: Array<any>;

  constructor(public statsService: StatsService) {
  }

  ngOnInit() {
  }

}
