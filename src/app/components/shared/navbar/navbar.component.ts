import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SidebarService} from '@app/services/sidebar.service';
import {Router} from '@angular/router';
import {EmployeService} from '@app/services/employe.service';
import {EmployeEditData} from '@app/models/EmployeEditData';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {EmpRefreshService} from '@app/services/emp-refresh.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Output() onCollapseActivated: EventEmitter<Boolean> = new EventEmitter<Boolean>();
  @Input() isChildViewActivated: boolean;
  @Output() backButtonEvent: EventEmitter<Boolean> = new EventEmitter<Boolean>();
  isCollapsed = false;

  profilePath = '/profile/' + localStorage.getItem('id');

  currentUser: EmployeEditData;

  constructor(public empService: EmployeService,
              private router: Router,
              private refreshService: EmpRefreshService,
              public navs: SidebarService) {
  }

  ngOnDestroy() {

  }

  ngOnInit() {
    this.init();
    this.refreshService.currentMessage.subscribe(
      m => {
        this.init();
      }
    );

  }

  onMenuButtonClicked() {
    this.isCollapsed = !this.isCollapsed;
    this.onCollapseActivated.emit(this.isCollapsed);
  }

  onPreviousClicked() {
    this.router.navigate(['../']);
  }

  init() {
    this.empService.getSelectedEmp(Number(localStorage.getItem('id'))).pipe(untilComponentDestroyed(this))
      .subscribe(
        user => {
          this.currentUser = user;
        }
      );
  }
}
