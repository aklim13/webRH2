import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {
  @Output() searchEvent = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  onSearch(event) {
    this.searchEvent.emit(event.target.value);
  }

  onBackspace(event) {
    if (event.target.value === '') {
      this.searchEvent.emit(event.target.value);
    }
  }
}
