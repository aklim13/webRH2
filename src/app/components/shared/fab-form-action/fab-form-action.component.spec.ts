import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FabFormActionComponent} from './fab-form-action.component';

describe('FabFormActionComponent', () => {
  let component: FabFormActionComponent;
  let fixture: ComponentFixture<FabFormActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FabFormActionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabFormActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
