import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormButton} from '@app/models/FormButton';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-fab-form-action',
  templateUrl: './fab-form-action.component.html',
  styleUrls: ['./fab-form-action.component.sass']
})
export class FabFormActionComponent implements OnInit {
  @Output() deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() addEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() updateEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() modeEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() reportEvent: EventEmitter<void> = new EventEmitter<void>();

  public buttons: FormButton[] = [
    {title: 'Ajouter', iconName: 'add'},
    {title: 'Modifier', iconName: 'mode_edit'},
    {title: 'Supprimer', iconName: 'delete'},
    {title: 'Mode', iconName: 'view_module'},
    {title: 'Rapport', iconName: 'insert_drive_file'}
  ];

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  onButtonClicked(event: any, title: string) {
    switch (title) {
      case 'Ajouter':
        this.handleActionButton('create', this.addEvent);
        break;
      case 'Modifier':
        this.updateEvent.emit();
        break;
      case 'Supprimer':
        this.deleteEvent.emit();
        break;
      case 'Rapport':
        this.reportEvent.emit();
        break;
      case 'Mode':
        this.buttons[4].iconName = (this.buttons[4].iconName === 'view_module') ? 'list' : 'view_module';
        this.modeEvent.emit();
        break;
    }
  }

  handleActionButton(path: string, eventEmitter: EventEmitter<void>) {
    if (this.router.url.includes(path)) {
      this.router.navigate(['../']);
    } else {
      this.router.navigate([path], {relativeTo: this.route});
    }
    eventEmitter.emit();
  }

}
