import {Component, Input, OnInit} from '@angular/core';
import {StatsService} from '@app/services/stats.service';
import {RowButtonType} from '@app/models/enums/RowButtonType';

@Component({
  selector: 'app-chart-donut',
  templateUrl: './chart-donut.component.html',
  styleUrls: ['./chart-donut.component.sass']
})
export class ChartDonutComponent implements OnInit {
  @Input() title = '';
  @Input() labels: string[];
  @Input() data: number[];
  @Input() type = 'doughnut';

  noRowButton = RowButtonType.NO_BUTTON;

  constructor(public statsService: StatsService) {
  }

  ngOnInit() {
  }


}
