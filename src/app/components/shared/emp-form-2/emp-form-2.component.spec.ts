import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmpForm2Component} from './emp-form-2.component';

describe('EmpForm2Component', () => {
  let component: EmpForm2Component;
  let fixture: ComponentFixture<EmpForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmpForm2Component]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
