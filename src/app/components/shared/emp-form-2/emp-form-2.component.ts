import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSlider, MatSliderChange} from '@angular/material';
import {FormService} from '@app/services/form.service';
import {EmployeService} from '@app/services/employe.service';
import {DataTable} from '@app/models/DataTable';
import {NzMessageService, UploadFile} from 'ng-zorro-antd';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeData} from '@app/models/EmployeData';
import {EmployeEditData} from '@app/models/EmployeEditData';
import {EmpRefreshService} from '@app/services/emp-refresh.service';

@Component({
  selector: 'app-emp-form-2',
  templateUrl: './emp-form-2.component.html',
  styleUrls: ['./emp-form-2.component.sass']
})
export class EmpForm2Component implements OnInit {
  @Input() employe: EmployeEditData;
  localedit: boolean;
  previewImage = '';
  isFieldRequired = false;

  formGroup: FormGroup;
  dataGroup: FormGroup;
  contactGroup: FormGroup;
  workGroup: FormGroup;
  otherGroup: FormGroup;

  echelleValue = 1;
  echellonValue = 1;

  @ViewChild('echelle') echelleSlider: MatSlider;
  @ViewChild('echellon') echellonSlider: MatSlider;

  employeData: DataTable = {} as DataTable;
  files: UploadFile [] = [];
  emp: EmployeEditData;

  constructor(private fb: FormBuilder,
              private msg: NzMessageService,
              public router: Router,
              private route: ActivatedRoute,
              private message: NzMessageService,
              private refreshService: EmpRefreshService,
              private employeService: EmployeService,
              public formService: FormService) {
  }

  get formArray(): AbstractControl | null {
    return this.formGroup.get('formArray');
  }

  ngOnInit() {
    this.route.params.subscribe(params => {

      this.initForm();

      this.employeService.getEmployeData().subscribe(
        res => {
          this.employeData = res;
        },
        error1 => {

        },
        () => {
          if (this.router.url.includes('edit')) {
            this.employeService.getSelectedEmp(params['matricule']).subscribe(
              (emp: EmployeEditData) => {
                this.emp = emp;
                this.initEditForm(emp);
              }
            );
          }
          this.initEditForm(this.employe);
        }
      );

    });
  }

  onFormSubmit() {

    if (this.router.url.includes('create')) {
      console.log(this.dataGroup.get('image').value.files[0]);
      this.employeService.postEmploye(this.otherGroup.get('admin').value,
        this.getSubmittedData(), this.dataGroup.get('image').value.files[0]).subscribe(
        data => {
        },
        err => {
          this.message.error('erreur' + err.error.message);
        },
        () => {
          this.message.success('compte a été crée avec success.');
          this.refreshService.changeMessage('refresh');
        }
      );
    }
    if (this.router.url.includes('edit') || this.employe) {
      const matricule = this.route.snapshot.params['matricule'];
      const image = this.dataGroup.get('image').value ? this.dataGroup.get('image').value.files[0] : undefined;
      this.employeService.updateEmploye(matricule, this.getSubmittedData(), image)
        .subscribe(
          res => {
            this.emp = res;
          },
          err => {
            console.log(err);
          },
          () => {
            this.message.success(`${this.emp.nom} ${this.emp.prenom} a été modifié avec succèss`);
            this.refreshService.changeMessage('refresh');
            this.employe = null;
          }
        );
    }
  }

  initEditForm(emp: EmployeEditData) {
    if (!emp) {
      return;
    }
    this.dataGroup.setValue({
      image: '',
      nom: emp.nom,
      prenom: emp.prenom,
      nombreenfant: emp.nombreEnfant,
      nomconjoint: emp.nomConjoint,
      prenomconjoint: emp.prenomConjoint,
      statusFamille: this.employeData.statusFamilles.filter(s => s.status === emp.statusFamille.status)[0].statusId,
      sexe: emp.sexe,
      dateNaissance: emp.dateNaissance
    });
    this.workGroup.setValue({
      salaire: emp.salaireBase,
      poste: this.employeData.postes.filter(s => s.poste === emp.poste)[0].posteId,
      grade: this.employeData.grades.filter(s => s.grade === emp.grade)[0].gradeId,
      service: this.employeData.services.filter(s => s.service === emp.service)[0].serviceId,
      echelle: emp.echelle,
      echellon: emp.echellon,
      dateRecrutement: emp.dateRecrutement
    });
    this.otherGroup.setValue({
      admin: emp.admin,
      cin: emp.cin,
      nmutuel: emp.nmutuel,
      nbank: emp.nbancaire
    });
    this.contactGroup.setValue({
      email: emp.email,
      tel: emp.tel,
      codepostal: emp.codePostal,
      adresse: emp.adresse
    });
    this.previewImage = emp.lien;
    this.echelleSlider.value = emp.echelle;
    this.echellonSlider.value = emp.echellon;
    this.echelleValue = emp.echelle;
    this.echellonValue = emp.echellon;
    this.formGroup.markAsDirty();
    this.formGroup.markAsTouched();
  }

  initForm() {
    this.dataGroup = this.fb.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      nombreenfant: ['', Validators.min(0)],
      nomconjoint: [''],
      prenomconjoint: [''],
      image: ['', [Validators.required]],
      statusFamille: [, Validators.required],
      sexe: [, Validators.required],
      dateNaissance: ['', Validators.required]
    });

    this.contactGroup = this.fb.group({
      email: ['', Validators.email],
      tel: ['06', Validators.required],
      codepostal: ['', Validators.required],
      adresse: ['', Validators.required],
    });

    this.workGroup = this.fb.group({
      salaire: ['', Validators.required],
      poste: [, Validators.required],
      grade: [, Validators.required],
      service: [, Validators.required],
      echelle: [1, Validators.required],
      echellon: [1, Validators.required],
      dateRecrutement: ['', Validators.required],
    });

    this.otherGroup = this.fb.group({
      admin: [false],
      cin: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      nmutuel: ['', Validators.compose([Validators.required, Validators.minLength(9)])],
      nbank: ['', Validators.compose([Validators.required, Validators.minLength(24)])]
    });

    this.formGroup = this.fb.group({
      formArray: this.fb.array([
        this.dataGroup,
        this.contactGroup,
        this.workGroup,
        this.otherGroup
      ])
    });
  }

  readUrl(events) {
    if (events.target.files && events.target.files[0]) {
      const reader = new FileReader();

      reader.onload = (event: any) => {
        this.previewImage = event.target.result;
      };

      reader.readAsDataURL(events.target.files[0]);
    }
  }

  onUpload() {
    const image: HTMLElement = document.getElementById('image') as HTMLElement;
    image.click();
    this.localedit = true;
  }

  resetForm() {
    this.localedit = false;
    this.formGroup.reset();
    this.previewImage = undefined;
    this.resetEchelleEchellon();
  }

  resetEchelleEchellon() {
    this.echelleSlider.value = 1;
    this.echellonSlider.value = 1;
    this.echellonValue = 1;
    this.echelleValue = 1;
  }

  onEchelleChange($event: MatSliderChange) {
    this.echelleValue = $event.value;

  }

  onEchellonChange($event: MatSliderChange) {
    this.echellonValue = $event.value;
  }

  onClick() {
    console.log(this.formGroup.value);
  }

  onStatusFamilleChange(event) {
    if (event === 2 || event === 4) {
      this.dataGroup.controls['nomconjoint'].enable();
      this.dataGroup.controls['prenomconjoint'].enable();
      this.dataGroup.controls['nomconjoint'].setValidators([Validators.required]);
      this.dataGroup.controls['prenomconjoint'].setValidators([Validators.required]);
      this.dataGroup.controls['nomconjoint'].updateValueAndValidity();
      this.dataGroup.controls['prenomconjoint'].updateValueAndValidity();
      this.isFieldRequired = true;
    } else {
      this.dataGroup.controls['nomconjoint'].setValidators([]);
      this.dataGroup.controls['nomconjoint'].setValue('-');
      this.dataGroup.controls['prenomconjoint'].setValidators([]);
      this.dataGroup.controls['prenomconjoint'].setValue('-');
      this.dataGroup.controls['nomconjoint'].disable();
      this.dataGroup.controls['prenomconjoint'].disable();
      this.isFieldRequired = false;
    }
  }

  private getSubmittedData(): EmployeData {
    return {
      adresse: this.contactGroup.get('adresse').value,
      nom: this.dataGroup.get('nom').value,
      prenom: this.dataGroup.get('prenom').value,
      sexe: this.dataGroup.get('sexe').value,
      email: this.contactGroup.get('email').value,
      dateRecrutement: this.workGroup.get('dateRecrutement').value,
      salaireBase: Number(this.workGroup.get('salaire').value),
      nombreEnfant: Number(this.dataGroup.get('nombreenfant').value),
      statusFamilleId: this.dataGroup.get('statusFamille').value,
      echelle: this.echelleValue,
      echellon: this.echellonValue,
      gradeId: this.workGroup.get('grade').value,
      posteId: this.workGroup.get('poste').value,
      serviceId: this.workGroup.get('service').value,
      prenomConjoint: this.dataGroup.get('prenomconjoint').value,
      nomConjoint: this.dataGroup.get('nomconjoint').value,
      codePostal: this.contactGroup.get('codepostal').value,
      tel: this.contactGroup.get('tel').value,
      dateNaissance: this.dataGroup.get('dateNaissance').value,
      admin: this.otherGroup.get('admin').value,
      cin: this.otherGroup.get('cin').value,
      mutuel: this.otherGroup.get('nmutuel').value,
      nbancaire: this.otherGroup.get('nbank').value
    };
  }
}
