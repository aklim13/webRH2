import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormButton} from '@app/models/FormButton';
import {RowButtonType} from '@app/models/enums/RowButtonType';

@Component({
  selector: 'app-form-action',
  templateUrl: './form-action.component.html',
  styleUrls: ['./form-action.component.sass']
})
export class FormActionComponent implements OnInit {
  @Input() buttonRowType: RowButtonType;
  @Output() deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() cancelEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() addEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() updateEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() modeEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() justifEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() acceptEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() checkingEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() refuseEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() reportEvent: EventEmitter<void> = new EventEmitter<void>();

  buttonsType = RowButtonType;

  public buttons: FormButton[] = [
    {id: 'conge_demande', title: 'Demander un congé', iconName: 'mail'},
    {id: 'add', title: 'Ajouter', iconName: 'add'},
    {id: 'edit', title: 'Modifier', iconName: 'mode_edit'},
    {id: 'justif', title: 'Justifier absence', iconName: 'mode_edit'},
    {id: 'delete', title: 'Supprimer', iconName: 'delete'},
    {id: 'accept', title: 'Accepter', iconName: 'check_circle'},
    {id: 'refuse', title: 'Refuser', iconName: 'cancel'},
    {id: 'anuller', title: 'Anuller', iconName: 'cancel'},
    {id: 'verify', title: 'Vérification', iconName: 'autorenew'},
    {id: 'mode', title: 'Trambinoscope', iconName: 'view_module'},
  ];

  constructor() {
  }

  ngOnInit() {
  }

  onButtonClicked(id: String) {
    switch (id) {
      case 'add':
        this.addEvent.emit();
        break;

      case 'edit':
        this.updateEvent.emit();
        break;

      case 'delete':
        this.deleteEvent.emit();
        break;

      case 'justif':
        this.justifEvent.emit();
        break;

      case 'Rapport':
        this.reportEvent.emit();
        break;

      case 'conge_demande':
        this.addEvent.emit();
        break;

      case 'anuller':
        this.cancelEvent.emit();
        break;

      case 'mode':
        const index = this.buttons.length - 1;
        if (this.buttons[index].iconName === 'view_module') {
          this.buttons[index].iconName = 'list';
          this.buttons[index].title = 'Liste';
        } else {
          this.buttons[index].iconName = 'view_module';
          this.buttons[index].title = 'Trambinoscope';
        }
        this.modeEvent.emit();
        break;

      case 'accept':
        this.acceptEvent.emit();
        break;

      case 'refuse':
        this.refuseEvent.emit();
        break;

      case 'verify':
        this.checkingEvent.emit();
        break;
    }
  }

  getButtons() {
    switch (+this.buttonRowType) {
      case  RowButtonType.EDIT :
        return this.buttons.filter(b => b.id === 'edit');

      case  RowButtonType.DELETE :
        return this.buttons.filter(b => b.id === 'delete');

      case  RowButtonType.ADD_EDIT_DELETE :
        return this.buttons.filter(b => b.iconName === 'add' || b.id === 'edit' || b.iconName === 'delete');

      case  RowButtonType.ADD_DELETE_REPORT :
        return this.buttons.filter(b => b.iconName === 'add' || b.iconName === 'delete');

      case  RowButtonType.EDIT_DELETE_REPORT :
        return this.buttons.filter(b => b.iconName === 'insert_drive_file' || b.id === 'edit' || b.iconName === 'delete');

      case  RowButtonType.NO_BUTTON :
        return [];

      case  RowButtonType.JUSTIF :
        return this.buttons.filter(b => b.id === 'justif');

      case  RowButtonType.DEMANDER_CONGE :
        return this.buttons.filter(b => b.iconName === 'mail' || b.id === 'anuller');

      case  RowButtonType.ACCEPT_REFUSE :
        return this.buttons.filter(b => b.iconName === 'check_circle' || b.id === 'refuse' || b.iconName === 'autorenew');

      case RowButtonType.ADD_EDIT_DELETE_MODE:
        return this.buttons.filter(b => b.id === 'add' || b.id === 'edit' || b.id === 'delete' || b.id === 'mode');

      default:
        return this.buttons.filter(b => b.id === 'add' || b.id === 'edit' || b.id === 'delete');
    }
  }
}
