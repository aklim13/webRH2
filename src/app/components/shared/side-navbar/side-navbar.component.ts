import {Component, Input, OnInit} from '@angular/core';
import {SidebarService} from '@app/services/sidebar.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.sass']
})
export class SideNavbarComponent implements OnInit {
  @Input() isCollapsed = false;
  navs;

  constructor(public sidebarService: SidebarService,
              public router: Router) {
    this.navs = this.sidebarService.getCurrentUserNav();
  }

  ngOnInit() {
  }


  onSubMenuChange(index: number) {
    for (const nav of this.navs) {
      nav.open = nav === this.navs[index];
    }
  }
}
