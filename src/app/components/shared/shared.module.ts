import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatOptionModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSliderModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {SearchComponent} from './search/search.component';
import {FabFormActionComponent} from './fab-form-action/fab-form-action.component';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {InputFileComponent} from './input-file/input-file.component';
import {RouterModule} from '@angular/router';
import {SecurePipe} from '@app/pipe/secure.pipe';
import {NgcFloatButtonModule} from 'ngc-float-button';
import {NavbarComponent} from '@app/components/shared/navbar/navbar.component';
import {SideNavbarComponent} from '@app/components/shared/side-navbar/side-navbar.component';
import {ListSideNavComponent} from '@app/components/shared/list-side-nav/list-side-nav.component';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {CardComponent} from './card/card.component';
import {FormActionComponent} from './form-action/form-action.component';
import {DatatablePageableComponent} from './datatable-pageable/datatable-pageable.component';
import {FormCardComponent} from './form-card/form-card.component';
import {UploadFileComponent} from './upload-file/upload-file.component';
import {EmpForm2Component} from '@app/components/shared/emp-form-2/emp-form-2.component';
import {ErrorRoleComponent} from './error-role/error-role.component';
import {ErrorNotFoundComponent} from './error-not-found/error-not-found.component';
import {ExceptionComponent} from './exception/exception.component';
import {ChartsModule} from 'ng2-charts';
import {ChartDonutComponent} from '@app/components/shared/chart-donut/chart-donut.component';
import {ChartSmallCardComponent} from './chart-small-card/chart-small-card.component';
import {ChartLineComponent} from './chart-line/chart-line.component';
import {StatsFormComponent} from './stats-form/stats-form.component';
import {EmpCardComponent} from './emp-card/emp-card.component';
import {DragulaModule} from 'ng2-dragula';
import {CardFormComponent} from './card-form/card-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxDatatableModule,
    ScrollToModule.forRoot(),
    NgZorroAntdModule,
    MatToolbarModule,
    MatChipsModule,
    MatCardModule,
    ChartsModule,
    MatIconModule,
    RouterModule,
    MatButtonModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    NgxMatSelectSearchModule,
    MatOptionModule,
    MatInputModule,
    MatCheckboxModule,
    MatMenuModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatSliderModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatSelectModule,
    MatStepperModule,
    MatProgressBarModule,
    MatTabsModule,
    DragulaModule,
    NgcFloatButtonModule,
    MatListModule,
  ],
  exports: [
    CommonModule,
    CardFormComponent,
    ChartSmallCardComponent,
    DragulaModule,
    NgxDatatableModule,
    NgZorroAntdModule,
    ExceptionComponent,
    FormsModule,
    StatsFormComponent,
    ChartsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    NgcFloatButtonModule,
    NgxMatSelectSearchModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatTabsModule,
    MatTooltipModule,
    MatOptionModule,
    MatDatepickerModule,
    EmpCardComponent,
    MatInputModule,
    InputFileComponent,
    SecurePipe,
    MatStepperModule,
    NgxDatatableModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    RouterModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSliderModule,
    ChartDonutComponent,
    MatMenuModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatExpansionModule,
    SearchComponent,
    MatChipsModule,
    FabFormActionComponent,
    UploadFileComponent,
    NavbarComponent,
    SideNavbarComponent,
    ListSideNavComponent,
    FormActionComponent,
    FormCardComponent,
    EmpForm2Component,
    ChartLineComponent,
    CardComponent,
    DatatablePageableComponent],
  declarations: [
    SearchComponent,
    FabFormActionComponent,
    InputFileComponent,
    SecurePipe,
    ChartDonutComponent,
    FormActionComponent,
    NavbarComponent,
    SideNavbarComponent,
    ListSideNavComponent,
    CardComponent,
    EmpForm2Component,
    FormActionComponent,
    DatatablePageableComponent,
    FormCardComponent,
    UploadFileComponent,
    ErrorRoleComponent,
    ErrorNotFoundComponent,
    ExceptionComponent,
    ChartSmallCardComponent,
    ChartLineComponent,
    StatsFormComponent,
    EmpCardComponent,
    CardFormComponent],
})
export class SharedModule {
}
