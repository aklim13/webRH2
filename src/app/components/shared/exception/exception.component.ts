import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-exception',
  templateUrl: './exception.component.html',
  styleUrls: ['./exception.component.sass']
})
export class ExceptionComponent implements OnInit {
  @Input() textError: string;
  @Input() errorNumber: number;

  constructor() {
  }

  ngOnInit() {
  }

}
