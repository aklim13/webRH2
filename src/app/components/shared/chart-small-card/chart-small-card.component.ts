import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-chart-small-card',
  templateUrl: './chart-small-card.component.html',
  styleUrls: ['./chart-small-card.component.sass']
})
export class ChartSmallCardComponent implements OnInit {
  @Input() title = '';
  @Input() icon = '';
  @Input() valeur = '';

  constructor() {
  }

  ngOnInit() {
  }

}
