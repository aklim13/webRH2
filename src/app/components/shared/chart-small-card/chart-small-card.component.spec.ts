import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChartSmallCardComponent} from './chart-small-card.component';

describe('ChartSmallCardComponent', () => {
  let component: ChartSmallCardComponent;
  let fixture: ComponentFixture<ChartSmallCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChartSmallCardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartSmallCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
