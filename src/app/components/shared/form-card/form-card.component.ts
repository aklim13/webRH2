import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-form-card',
  templateUrl: './form-card.component.html',
  styleUrls: ['./form-card.component.sass']
})
export class FormCardComponent implements OnInit, OnChanges {
  @Input() progress = 0;
  @Input() paddForm = true;
  progressChange = 0;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.progressChange = this.progress;
  }

}
