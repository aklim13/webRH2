import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DatatablePageableComponent} from './datatable-pageable.component';

describe('DatatablePageableComponent', () => {
  let component: DatatablePageableComponent;
  let fixture: ComponentFixture<DatatablePageableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatatablePageableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatablePageableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
