import {
  AfterContentChecked,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {DatatableComponent, TableColumn} from '@swimlane/ngx-datatable';
import {Page} from '@app/models/Page';
import {DataGridOptions} from '@app/models/datagrid';
import {TableType} from '@app/models/enums/TableType';
import {Observable} from 'rxjs/Observable';
import {ProfileService} from '@app/services/profile.service';
import {ActivatedRoute, Router} from '@angular/router';
import {untilComponentDestroyed} from 'ng2-rx-componentdestroyed';
import {saveAs as importedSaveAs} from 'file-saver';
import {Document} from '@app/models/Document';
import {EmployeService} from '@app/services/employe.service';
import {CongeService} from '@app/services/conge.service';
import {UserService} from '@app/services/user.service';
import {AbsenceService} from '@app/services/absence.service';
import {CompetenceService} from '@app/services/competence.service';
import {PrimeService} from '@app/services/prime.service';
import {LanguageService} from '@app/services/language.service';
import {PosteService} from '@app/services/poste.service';

@Component({
  selector: 'app-datatable-pageable',
  templateUrl: './datatable-pageable.component.html',
  styleUrls: ['./datatable-pageable.component.sass']
})
export class DatatablePageableComponent implements OnInit, OnDestroy, AfterContentChecked, OnChanges, DataGridOptions {
  @Input() footerText = 'Total';
  @Input() isEmailPresent = false;
  @Input() isDownload = false;
  @Input() isConge = false;
  @Input() columns: TableColumn[];
  @Input() tableType: TableType;
  @Input() query: string;
  @Input() isImage = false;

  @ViewChild('tableWrapper') tableWrapper;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @Output() pageEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectEvent: EventEmitter<any> = new EventEmitter<any>();
  page = new Page();
  loading = true;
  data: any[];
  selected = [];
  rows: Observable<any>;
  matricule: number;
  private latestWidth: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private congeService: CongeService,
              private userService: UserService,
              private competenceService: CompetenceService,
              private absenceService: AbsenceService,
              private primeService: PrimeService,
              private posteService: PosteService,
              private languageService: LanguageService,
              private employeService: EmployeService,
              private profileService: ProfileService) {
  }

  ngOnInit() {
    this.onPage({offset: 0});
  }

  ngOnDestroy(): void {
  }

  onDownloadClicked(row: Document) {
    this.profileService.downloadDocument(row.lien).subscribe(
      blob => {
        importedSaveAs(blob, row.origin);
      }
    );
  }

  onClick(event): void {
    if (event.type === 'click') {
      this.clickEvent.emit();
    }
  }

  onPage(event): void {
    this.route.params.subscribe(
      params => {
        this.matricule = params['matricule'];
        this.page.pageNumber = event.offset;
        this.onSelect({selected: []});
        this.query = this.query ? this.query : '';
        switch (this.tableType) {

          case TableType.ProfileContact:
            this.rows = this.profileService.getUserContacts(this.matricule, this.query, this.page);
            break;

          case TableType.ProfileExp:
            this.rows = this.profileService.getUserExperiences(this.matricule, this.query, this.page);
            break;

          case TableType.ProfileHistorique:
            this.rows = this.profileService.getUserHistorique(this.matricule, this.query, this.page);
            break;

          case TableType.ProfilePrime:
            this.rows = this.profileService.getUserPrime(this.matricule, this.query, this.page);
            break;

          case TableType.ProfileLanguage:
            this.rows = this.profileService.getUserLanguages(this.matricule, this.page);
            break;

          case TableType.ProfileDocument:
            this.rows = this.profileService.getUserDocuments(this.matricule, this.query, this.page);
            break;

          case TableType.Employe:
            this.rows = this.employeService.getPreviewEmployees(this.page, this.query);
            break;

          case TableType.CongeDemandes:
            this.rows = this.congeService.getCongeDemandes(this.query, this.page);
            break;

          case TableType.Language:
            this.rows = this.languageService.getLanguages(this.page, this.query);
            break;

          case TableType.Postes:
            this.rows = this.posteService.getAllPostes(this.page);
            break;

          case TableType.Competences:
            this.rows = this.competenceService.getCompetencesPageable(this.page, this.query);
            break;

          case TableType.Services:
            this.rows = this.employeService.getServices(this.page);
            break;

          case TableType.Prime:
            this.rows = this.primeService.getPrimesPageable(this.page, this.query);
            break;

          case TableType.CongeHistorique:
            this.rows = this.congeService.getCongeHistorique(this.query, this.page);
            break;

          case TableType.AbsenceHistorique:
            this.rows = this.absenceService.getAbsenceHistorique(this.query, this.page);
            break;

          case TableType.CongeMesDemandes:
            this.rows = this.congeService.getUserCongeDemandes(this.userService.getCurrentUserId(), this.query, this.page);
            break;

          case TableType.AbsenceHistoriqueEmp:
            this.rows = this.absenceService.getEmployeAbsenceHistorique(this.userService.getCurrentUserId(), this.query, this.page);
            break;
        }

        this.rows
          .pipe(untilComponentDestroyed(this))
          .subscribe(
            data => {
              console.log(this.data);
              this.page.pageNumber = data.pageable.offset;
              this.page.size = data.pageable.pageSize;
              this.page.totalPages = data.totalPages;
              this.page.totalElements = data.totalElements;
              this.data = [...data.content];
            },
            err => {
              console.log(err);
              this.loading = false;
            },
            () => {
              this.loading = false;
              this.pageEvent.emit(this.page);
            }
          );
      }
    );

  }

  onSelect({selected}): void {
    console.log(this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.selectEvent.emit({selected: this.selected});
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onPage({offset: 0});
  }

  onProfile(row) {
    this.router.navigate(['/profile', row.matricule]);
  }

  ngAfterContentChecked() {
    if (this.table && this.table.element.clientWidth !== this.latestWidth) {
      this.latestWidth = this.table.element.clientWidth;
      this.table.recalculate();
    }
  }

}
