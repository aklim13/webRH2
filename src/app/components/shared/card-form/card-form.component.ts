import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.sass']
})
export class CardFormComponent implements OnInit, OnChanges {
  @Output() formSubmitted = new EventEmitter<any>();
  @Input() formLayout;
  @Input() buttonText;
  @Input() formGroup: FormGroup;

  constructor() {
  }

  ngOnInit() {
  }

  onReset() {
    this.formGroup.reset();
    this.buttonText = 'Ajouter';
  }
  onFormSubmit() {
    this.formSubmitted.emit(this.formGroup.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes.formGroup);
  }

}
