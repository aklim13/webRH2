import {Component, OnInit} from '@angular/core';
import {SidebarService} from '@app/services/sidebar.service';

@Component({
  selector: 'app-list-side-nav',
  templateUrl: './list-side-nav.component.html',
  styleUrls: ['./list-side-nav.component.sass']
})
export class ListSideNavComponent implements OnInit {

  constructor(public sidebarService: SidebarService) {
  }

  ngOnInit() {
  }
}
