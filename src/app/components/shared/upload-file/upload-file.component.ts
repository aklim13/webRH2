import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {UploadFile} from 'ng-zorro-antd';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.sass']
})
export class UploadFileComponent implements OnInit, OnChanges {
  @Input() reset: Boolean = false;
  @Input() title: String = '';
  files: UploadFile [] = [];
  @Output() uploadFileEvent: EventEmitter<any> = new EventEmitter<any>();

  beforeUpload = (file: UploadFile): boolean => {
    this.uploadFileEvent.emit({file: file, title: this.title});
    this.files = [];
    this.files.push(file);
    return false;
  };

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.reset) {
      this.files = [];
    }
  }

  onFileListChange($event) {
    this.uploadFileEvent.emit({reset: true});
  }
}
