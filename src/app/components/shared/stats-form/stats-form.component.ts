import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-stats-form',
  templateUrl: './stats-form.component.html',
  styleUrls: ['./stats-form.component.sass']
})
export class StatsFormComponent implements OnInit {
  dateRange;
  days = 3;
  @Output() daysEvent = new EventEmitter<number>();
  @Output() dateEvent = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  onChange(result: Date): void {
    const datePipe = new DatePipe('en_us');
    this.dateEvent.emit({
      beginDate: datePipe.transform(result[0], 'yyyy-MM-dd'),
      endDate: datePipe.transform(result[1], 'yyyy-MM-dd')
    });
  }

  onDaysSubmit() {
    this.daysEvent.emit(this.days);
  }
}
