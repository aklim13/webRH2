import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-emp-card',
  templateUrl: './emp-card.component.html',
  styleUrls: ['./emp-card.component.sass']
})
export class EmpCardComponent implements OnInit {
  @Input() image: string;
  @Input() fullName: string;
  @Input() buttonText = 'voir profile';
  @Output() cardEvent = new EventEmitter<any>();
  @Output() buttonEvent = new EventEmitter<any>();
  @Input() showButton = false;
  selected = false;

  constructor() {
  }

  ngOnInit() {
  }

  onCardClicked() {
    if (this.showButton) {
      this.selected = !this.selected;
    }
    this.cardEvent.emit();
  }

  onButtonClicked() {
    this.buttonEvent.emit();
  }
}
