import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {OnAdd, OnDelete, OnEdit, OnMode, OnReport} from '@app/models/events/actions';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})
export class CardComponent implements OnInit, OnAdd, OnDelete, OnEdit, OnMode, OnReport {
  @Input() title: String = '';
  @Input() work: String = '';
  @Input() moreMargin: Boolean = false;
  @Input() buttonType: RowButtonType;

  @Output() deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() addEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() justifEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() updateEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() cancelEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() acceptEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() checkingEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() refuseEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() modeEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() reportEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit() {
  }

  onAdd() {
    this.addEvent.emit();
  }

  onDelete() {
    this.deleteEvent.emit();
  }

  onJustif() {
    this.justifEvent.emit();
  }

  onMode() {
    this.modeEvent.emit();
  }

  onCancel() {
    this.cancelEvent.emit();
  }

  onReport() {
    this.reportEvent.emit();
  }

  onEdit() {
    this.updateEvent.emit();
  }

  onAccept() {
    this.acceptEvent.emit();
  }

  onRefuse() {
    this.refuseEvent.emit();
  }

  onCheck() {
    this.checkingEvent.emit();
  }
}
