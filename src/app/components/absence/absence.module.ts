import {NgModule} from '@angular/core';
import {SharedModule} from '@app/components/shared/shared.module';
import {AbsenceRoutingModule} from '@app/components/absence/absence-routing.module';
import {NoterAbsenceComponent} from './noter-absence/noter-absence.component';
import {HistoriqueAbsenceComponent} from './historique-absence/historique-absence.component';
import {JustifierAbsenceComponent} from './justifier-absence/justifier-absence.component';
import {HistoriqueAbsenceEmpComponent} from './historique-absence-emp/historique-absence-emp.component';
import {AbsenceStatsComponent} from './absence-stats/absence-stats.component';
import {NoterAbsenceAutoComponent} from './noter-absence-auto/noter-absence-auto.component';

@NgModule({
  imports: [
    SharedModule,
    AbsenceRoutingModule
  ],
  exports: [
    SharedModule
  ],
  declarations: [NoterAbsenceComponent, HistoriqueAbsenceComponent, JustifierAbsenceComponent, HistoriqueAbsenceEmpComponent, AbsenceStatsComponent, NoterAbsenceAutoComponent]
})
export class AbsenceModule {
}
