import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NoterAbsenceComponent} from './noter-absence.component';

describe('NoterAbsenceComponent', () => {
  let component: NoterAbsenceComponent;
  let fixture: ComponentFixture<NoterAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NoterAbsenceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoterAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
