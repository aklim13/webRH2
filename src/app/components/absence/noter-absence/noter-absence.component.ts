import {Component, OnInit} from '@angular/core';
import {EmployeService} from '@app/services/employe.service';
import {EmployePreviewData} from '@app/models/EmployePreviewData';
import {NzMessageService} from 'ng-zorro-antd';
import {AbsenceService} from '@app/services/absence.service';

@Component({
  selector: 'app-noter-absence',
  templateUrl: './noter-absence.component.html',
  styleUrls: ['./noter-absence.component.sass']
})
export class NoterAbsenceComponent implements OnInit {
  employeList: EmployePreviewData[] = [];
  absenceList: EmployePreviewData[] = [];

  constructor(private employeService: EmployeService,
              private absenceService: AbsenceService,
              private message: NzMessageService) {
  }

  ngOnInit() {
    this.refreshEmploye('');
  }

  refreshEmploye(query: string) {
    this.employeService.getAllEmployees(query).subscribe(
      res => {
        this.employeList = res;
      }
    );
  }

  onCardClicked(employe: EmployePreviewData) {
    this.absenceList.push(employe);
    this.employeList.splice(this.employeList.indexOf(employe), 1);
  }

  onCardRemoved(employe: EmployePreviewData) {
    this.absenceList.splice(this.absenceList.indexOf(employe), 1);
    this.employeList.push(employe);
  }

  onAbsenceNoted() {
    this.absenceService.addAbsences(this.absenceList.map(absence => absence.matricule)).subscribe(
      res => {
        this.message.success('Absence ajouté avec success');
        this.refreshEmploye('');
        this.absenceList = [];
      }
    );
  }

  onSearchEvent(query: string) {
    this.employeService.getAllEmployees(query).subscribe(
      res => {
        this.employeList = res.filter(e => !this.absenceList.map(e1 => e1.matricule).includes(e.matricule));
      }
    );
  }
}
