import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '@app/services/user.service';
import {BaseChartComponent} from '@swimlane/ngx-charts';
import {StatsService} from '@app/services/stats.service';

@Component({
  selector: 'app-absence-stats',
  templateUrl: './absence-stats.component.html',
  styleUrls: ['./absence-stats.component.sass']
})
export class AbsenceStatsComponent implements OnInit {
  isData = false;
  absenceStats: AbsenceStats;
  @ViewChild(BaseChartComponent) private chart;

  constructor(private userService: UserService,
              private statsService: StatsService) {

  }

  ngOnInit() {
    this.getAbsenceByDays(7);
  }

  onAbsenceDays(days: number) {
    this.getAbsenceByDays(days);
  }

  refreshChart() {
    if (this.chart !== undefined) {
      this.chart.ngOnDestroy();
      this.chart.chart = this.chart.getChartBuilder(this.chart.ctx);
    }
  }

  getAbsenceByDays(days: number) {
    if (this.userService.isAdmin()) {
      this.statsService.getAbsenceStatsByDays(days).subscribe(
        stats => {
          this.absenceStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    } else {
      this.statsService.getCurrentUserAbsenceStatsByDays(days).subscribe(
        stats => {
          this.absenceStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    }
  }

  onAbsenceDate(res: any) {
    if (this.userService.isAdmin()) {
      this.statsService.getAbsencestatsByDate(res.beginDate, res.endDate).subscribe(
        stats => {
          this.absenceStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    } else {
      this.statsService.getCurrentUserAbsencestatsByDate(res.beginDate, res.endDate).subscribe(
        stats => {
          this.absenceStats = stats;
          this.refreshChart();
          this.isData = true;
        }
      );
    }
  }

}
