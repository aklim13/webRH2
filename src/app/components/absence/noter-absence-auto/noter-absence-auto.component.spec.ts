import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NoterAbsenceAutoComponent} from './noter-absence-auto.component';

describe('NoterAbsenceAutoComponent', () => {
  let component: NoterAbsenceAutoComponent;
  let fixture: ComponentFixture<NoterAbsenceAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NoterAbsenceAutoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoterAbsenceAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
