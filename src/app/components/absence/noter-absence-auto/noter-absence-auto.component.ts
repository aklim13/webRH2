import {Component, OnInit} from '@angular/core';
import {AbsenceService} from '@app/services/absence.service';
import {Router} from '@angular/router';
import {Presence} from '@app/models/Presence';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-noter-absence-auto',
  templateUrl: './noter-absence-auto.component.html',
  styleUrls: ['./noter-absence-auto.component.sass']
})
export class NoterAbsenceAutoComponent implements OnInit {
  presenceList: Presence[] = [];

  constructor(
    private router: Router,
    private absenceService: AbsenceService,
    private message: NzMessageService
  ) {
  }

  ngOnInit() {
    this.absenceService.getPresences('').subscribe(
      res => {
        this.presenceList = res;
      }
    );
  }

  onCardClicked(employe: Presence) {
    this.router.navigate(['/profile', employe.matricule]);
  }

  onSearchEvent(query: string) {
    this.absenceService.getPresences(query).subscribe(
      res => {
        this.presenceList = res;
      }
    );
  }

  onAbsenceNoted() {
    this.absenceService.addAbsencesAuto().subscribe(
      res => {
        this.message.success('Absence enregistré avec success');
      }
    );
  }
}
