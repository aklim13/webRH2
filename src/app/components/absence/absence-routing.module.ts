import {NgModule} from '@angular/core';
import {AuthGuard} from '@app/guards/auth.guard';
import {RoleGuard} from '@app/guards/role.guard';
import {RouterModule, Routes} from '@angular/router';
import {NoterAbsenceComponent} from '@app/components/absence/noter-absence/noter-absence.component';
import {HistoriqueAbsenceComponent} from '@app/components/absence/historique-absence/historique-absence.component';
import {HistoriqueAbsenceEmpComponent} from '@app/components/absence/historique-absence-emp/historique-absence-emp.component';
import {AbsenceStatsComponent} from '@app/components/absence/absence-stats/absence-stats.component';
import {NoterAbsenceAutoComponent} from '@app/components/absence/noter-absence-auto/noter-absence-auto.component';

const absenceRoutes: Routes = [
  {
    path: 'absence/noter',
    component: NoterAbsenceComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin']}
  },
  {
    path: 'presence',
    component: NoterAbsenceAutoComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin']}
  },
  {
    path: 'absence/historique',
    component: HistoriqueAbsenceComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin']}
  },
  {
    path: 'absence/statistique',
    component: AbsenceStatsComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['admin', 'employe']}
  },
  {
    path: 'absence/historique/me',
    component: HistoriqueAbsenceEmpComponent,
    canActivate: [AuthGuard, RoleGuard],
    canActivateChild: [AuthGuard, RoleGuard],
    data: {roles: ['employe']}
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(absenceRoutes)
  ],
  exports: [RouterModule]
})
export class AbsenceRoutingModule {
}
