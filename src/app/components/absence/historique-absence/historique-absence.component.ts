import {Component, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {TableType} from '@app/models/enums/TableType';
import {RowButtonType} from '@app/models/enums/RowButtonType';

@Component({
  selector: 'app-historique-absence',
  templateUrl: './historique-absence.component.html',
  styleUrls: ['./historique-absence.component.sass']
})
export class HistoriqueAbsenceComponent implements OnInit {
  tableType = TableType.AbsenceHistorique;
  rowButtonType = RowButtonType.NO_BUTTON;
  query: string;
  @ViewChild('datatable') dataTable;
  columns: TableColumn[] = [
    {prop: 'fullname', name: 'Nom complet'},
    {prop: 'dateAbsence', name: 'Date d\'absence'},
    {prop: 'dateJustification', name: 'Date de justification'},
    {prop: 'raison', name: 'Raison'}
  ];

  constructor() {
  }

  ngOnInit() {
  }

  onSearch(query: string) {
    this.query = query;
  }

  onChange() {
    this.refresh();
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }
}
