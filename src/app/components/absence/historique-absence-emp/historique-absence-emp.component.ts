import {Component, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';
import {TableType} from '@app/models/enums/TableType';
import {RowButtonType} from '@app/models/enums/RowButtonType';
import {FormService} from '@app/services/form.service';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-historique-absence-emp',
  templateUrl: './historique-absence-emp.component.html',
  styleUrls: ['./historique-absence-emp.component.sass']
})
export class HistoriqueAbsenceEmpComponent implements OnInit {
  tableType = TableType.AbsenceHistoriqueEmp;
  rowButtonType = RowButtonType.JUSTIF;
  showForm = {add: false};
  query: string;
  @ViewChild('datatable') dataTable;
  columns: TableColumn[] = [
    {prop: 'absenceId', name: 'ID'},
    {prop: 'fullname', name: 'Nom complet'},
    {prop: 'dateAbsence', name: 'Date d\'absence'},
    {prop: 'dateJustification', name: 'Date de justification'},
    {prop: 'raison', name: 'Raison'}
  ];
  absenceId = 0;

  constructor(private formService: FormService,
              private message: NzMessageService) {
  }

  ngOnInit() {
  }

  onAbsenceChange() {
    this.refresh();
  }

  onSearch(query: string) {
    this.query = query;
  }

  onChange() {
    this.refresh();
  }

  refresh() {
    try {
      this.dataTable.onPage({offset: 0});
    } catch (e) {
    }
  }

  onUpdate() {
    if (this.absenceId) {
      this.formService.showAddForm(this.showForm);
    } else {
      this.message.info('Veuillez selectionner un seul absence à justifier.');
    }
  }

  onSelect(s): void {
    if (s.selected.length === 1) {
      this.absenceId = s.selected[0].absenceId;
    }
  }
}
