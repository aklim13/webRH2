import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HistoriqueAbsenceEmpComponent} from './historique-absence-emp.component';

describe('HistoriqueAbsenceEmpComponent', () => {
  let component: HistoriqueAbsenceEmpComponent;
  let fixture: ComponentFixture<HistoriqueAbsenceEmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HistoriqueAbsenceEmpComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriqueAbsenceEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
