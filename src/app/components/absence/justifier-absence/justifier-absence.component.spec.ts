import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {JustifierAbsenceComponent} from './justifier-absence.component';

describe('JustifierAbsenceComponent', () => {
  let component: JustifierAbsenceComponent;
  let fixture: ComponentFixture<JustifierAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JustifierAbsenceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JustifierAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
