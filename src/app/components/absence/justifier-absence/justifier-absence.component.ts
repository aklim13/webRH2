import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AbsenceService} from '@app/services/absence.service';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-justifier-absence',
  templateUrl: './justifier-absence.component.html',
  styleUrls: ['./justifier-absence.component.sass']
})
export class JustifierAbsenceComponent implements OnInit {
  absenceForm: FormGroup;
  @Input() edit: Boolean;
  @Input() absenceId: number;
  uploadedFiles = [];
  reset = 1;
  @Output() absenceChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private message: NzMessageService,
              private absenceService: AbsenceService) {
  }

  ngOnInit() {
    this.absenceForm = this.fb.group({
      raison: ['', Validators.required]
    });
  }

  onFormSubmit() {
    this.absenceService.justifyAbsence(this.absenceId, this.uploadedFiles[0], this.absenceForm.get('raison').value).subscribe(
      res => {
        this.message.info('Justification absence ajouté.');
        this.absenceChanged.emit();
      }
    );
  }

  onResetClicked() {
    this.reset++;
    this.absenceForm.reset();
    this.uploadedFiles = [];
  }

  onFileContent(event: any) {
    if (event.reset) {
      this.onResetClicked();
    } else {
      this.uploadedFiles.push(event);
    }
  }
}
