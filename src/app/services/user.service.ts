import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class UserService {
  private jwt: JwtHelperService = new JwtHelperService();

  constructor() {
  }

  getCurrentUserRole(): string {
    return this.jwt.decodeToken(localStorage.getItem('token')).roles[0];
  }

  isAdmin(): boolean {
    return this.getCurrentUserRole().toLowerCase() === 'admin';
  }

  getCurrentUserId(): number {
    return this.jwt.decodeToken(localStorage.getItem('token')).sub;
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return token && !this.jwt.isTokenExpired(token);
  }
}
