import {Injectable} from '@angular/core';
import {UtilsService} from '@app/services/utils.service';
import {HttpClient} from '@angular/common/http';
import {Poste} from '@app/models/DataTable';
import {Page} from '@app/models/Page';

@Injectable()
export class PosteService {

  constructor(private http: HttpClient,
              private utils: UtilsService) {
  }

  getAllPostes(page: Page) {
    return this.http.get(`/settings/postes/?page=${page.pageNumber}&size=${page.size}`);
  }

  addPoste(poste: Poste) {
    return this.http.post('/settings/postes/', poste);
  }

  updatePoste(poste: Poste) {
    return this.http.put('/settings/postes/' + poste.posteId, poste);
  }

  deletePostes(ids: number[]) {
    return this.http.delete('/settings/postes/' + this.utils.getIds(ids));
  }
}
