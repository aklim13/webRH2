import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Page} from '@app/models/Page';
import {TypeConge} from '@app/models/TypeConge';
import {CongeDemander} from '@app/models/lists/CongeList';

@Injectable()
export class CongeService {

  constructor(private http: HttpClient) {
  }

  deleteConge(ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return this.http.delete(`/conges/${result.slice(0, -1)}`);
  }

  demandeConge(demande: CongeDemander) {
    return this.http.post(`/conges`, demande);
  }

  getTypeConges() {
    return this.http.get<TypeConge[]>('/settings/typeconges');
  }

  getCongeDemandes(query: string, page: Page) {
    return this.http.get(`/conges/demandes?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getUserCongeDemandes(matricule: number, query: string, page: Page) {
    return this.http.get(`/conges/${matricule}/users?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  cancelConge(id: number, matricule: number) {
    return this.http.put(`/conges/${matricule}/cancel?congeId=` + id, {});
  }

  answerConge(ids: number[], answer: String) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return this.http.put(`/conges/answer?etat=${answer}&ids=${result.slice(0, -1)}`, {});
  }

  getCongeHistorique(query: string, page: Page) {
    return this.http.get(`/conges?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }
}
