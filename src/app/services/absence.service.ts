import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Page} from '@app/models/Page';
import {Presence} from '@app/models/Presence';

@Injectable()
export class AbsenceService {

  constructor(private http: HttpClient) {
  }

  getAbsenceHistorique(query: string, page: Page) {
    return this.http.get(`/absences?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getEmployeAbsenceHistorique(matricule: number, query: string, page: Page) {
    return this.http.get(`/absences/${matricule}/users/?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  addAbsences(ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    result = result.slice(0, -1);
    return this.http.post(`/absences/${result}`, {});
  }


  addAbsencesAuto() {
    return this.http.post(`/absences/auto/`, {});
  }

  justifyAbsence(absenceId: number, file, raison: string) {
    const formData = new FormData();
    formData.append('raison', raison);
    formData.append('file', file.file);
    return this.http.put(`/absences/${absenceId}`, formData);
  }

  getPresences(query: string) {
    return this.http.get<Presence[]>('/absences/presences/?q=' + query);
  }
}
