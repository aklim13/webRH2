import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EmployeList} from '@app/models/lists/EmployeList';
import {Page} from '@app/models/Page';
import {EmployeData} from '@app/models/EmployeData';
import 'rxjs/add/operator/combineLatest';
import {Observable} from 'rxjs/Observable';
import {DataTable, Service} from '@app/models/DataTable';
import {EmployeEditData} from '@app/models/EmployeEditData';
import {EmployePreviewData} from '@app/models/EmployePreviewData';
import {UtilsService} from '@app/services/utils.service';

@Injectable()
export class EmployeService {

  constructor(private http: HttpClient, private utils: UtilsService) {
  }

  getServices(page: Page) {
    return this.http.get(`/settings/services/?page=${page.pageNumber}&size=${page.size}`);
  }

  deleteServices(ids: number[]) {
    return this.http.delete(`/settings/services/` + ids);
  }

  addService(service: string) {
    return this.http.post('/settings/services/', {service: service});
  }

  updateService(service: Service) {
    return this.http.put('/settings/services/' + service.serviceId, {service: service.service});
  }

  getAllEmployees(query: string) {
    return this.http.get<EmployePreviewData[]>(`/employees?q=${query}`);
  }

  getPreviewEmployees(page: Page, query: string): Observable<EmployeList> {
    return this.http.get<EmployeList>(`/employees/all?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  postEmploye(adminRole: boolean, data: EmployeData, image) {
    const formData = new FormData();
    formData.append('doc', 'PHOTO');
    formData.append('user', JSON.stringify(data));
    formData.append('file', image, image.name);
    if (adminRole) {
      return this.http.post('/auth/create/admin', formData);
    }
    return this.http.post('/auth/create/employe', formData);
  }

  deleteEmploye(ids: number[]) {
    return this.http.delete(`/employees/${this.utils.getIds(ids)}`);
  }

  deleteOneEmploye(id: number) {
    return this.http.delete(`/employees/${id}`);
  }


  getEmployeData() {
    return this.http.get<DataTable>('/employees/data');
  }

  getSelectedEmp(matricule: number) {
    return this.http.get(`/employees/${matricule}`);
  }

  updateEmploye(matricule: number, data: EmployeData, image) {
    const formData = new FormData();
    formData.append('employe', JSON.stringify(data));
    if (image) {
      formData.append('file', image, image.name);
    }
    return this.http.put<EmployeEditData>(`/employees/${matricule}`, formData);
  }
}

