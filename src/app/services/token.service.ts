import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class TokenService {
  private jwt: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient) {
  }

  shouldIGetToken(threshold_seconds: number = 120): boolean {
    const expDate = this.jwt.getTokenExpirationDate(localStorage.getItem('token'))
      .valueOf() - (threshold_seconds * 1000);
    return new Date().valueOf() > expDate;
  }

  refreshToken() {
    return this.http.post<any>(`/auth/refresh`, {})
      .map(user => {
        this.saveToken(user);
        return user;
      });
  }

  saveToken(user: any) {
    if (user && user.accessToken) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('token', user.accessToken);
      const decodedToken = this.jwt.decodeToken(user.accessToken);
      localStorage.setItem('id', decodedToken.sub);
    }
  }

}
