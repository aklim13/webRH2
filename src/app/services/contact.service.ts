import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Contact} from '@app/models/Contact';
import {EmployeEditData} from '@app/models/EmployeEditData';

@Injectable()
export class ContactService {

  constructor(private http: HttpClient) {
  }

  addContact(contact: Contact, matricule: number) {
    return this.http.post(`/profile/${matricule}/contacts`, contact);
  }

  updateContact(contact: Contact, matricule: number) {
    return this.http.put<EmployeEditData>(`/profile/${matricule}/contacts/${contact.contactId}`, contact);
  }

  deleteEmploye(matricule: number, ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return this.http.delete(`/profile/${matricule}/contacts/${result.slice(0, -1)}`);
  }

}
