import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Competence} from '@app/models/Competence';
import {CompetenceList} from '@app/models/lists/CompetenceList';
import {Page} from '@app/models/Page';
import {UtilsService} from '@app/services/utils.service';

@Injectable()
export class CompetenceService {

  constructor(private http: HttpClient, private utils: UtilsService) {
  }

  getCompetencesPageable(page: Page, query: string) {
    return this.http.get<CompetenceList>(`/settings/competences?page=${page.pageNumber}&size=${page.size}`);
  }

  addCompetence(competence: string) {
    return this.http.post('/settings/competences', {competence: competence});
  }

  editCompetence(c: Competence) {
    return this.http.put('/settings/competences/' + c.comptenceId, {competence: c.competence});
  }

  deleteCompetences(ids: number[]) {
    return this.http.delete('/settings/competences/' + this.utils.getIds(ids));
  }

  getUserCompetences(matricule: number) {
    return this.http.get<Competence[]>(`/profile/${matricule}/competences`);
  }

  getCompetences(matricule: number) {
    return this.http.get<Competence[]>(`/profile/${matricule}/competences/new`);
  }

  postCompetence(matricule: number, ids: number[]) {
    return this.http.post(`/profile/${matricule}/competences/${this.utils.getIds(ids)}`, '');
  }

  deleteCompetence(matricule: number, ids: number[]) {
    return this.http.delete(`/profile/${matricule}/competences/${this.utils.getIds(ids)}`);
  }
}
