import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PasswordReq} from '@app/models/PasswordReq';

@Injectable()
export class PasswordService {

  constructor(private http: HttpClient) {
  }

  requestNewPassword(username: string) {
    return this.http.get('/password/forgot-password?username=' + username);
  }

  resetPassword(password: string, token: string) {
    return this.http.put<any>('/password/reset-password/?token=' + token, {password: password, confirmPassword: password});
  }

  changePassword(passwordReq: PasswordReq) {
    return this.http.put('/password/change-password', passwordReq);
  }
}
