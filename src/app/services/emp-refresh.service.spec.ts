import {inject, TestBed} from '@angular/core/testing';

import {EmpRefreshService} from './emp-refresh.service';

describe('EmpRefreshService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmpRefreshService]
    });
  });

  it('should be created', inject([EmpRefreshService], (service: EmpRefreshService) => {
    expect(service).toBeTruthy();
  }));
});
