import {Injectable} from '@angular/core';

@Injectable()
export class FormService {

  constructor() {
  }

  showAddForm(b) {
    b.add = !b.add;
    if (b.edit) {
      b.add = true;
      b.edit = !b.add;
    }
  }

  showEditForm(b) {
    b.edit = !b.edit;
    if (b.add) {
      b.edit = true;
      b.add = !b.add;
    }
  }

  onNumericInput($event) {
    const pattern = /[0-9.\+\-\ ]/;
    const inputChar = String.fromCharCode($event.charCode);

    if (!pattern.test(inputChar)) {
      $event.preventDefault();
    }
  }

  isEmailValid(emailForm) {
    return emailForm.get('email').hasError('email') && !emailForm.get('email').hasError('required');
  }

  onInput($event) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode($event.charCode);

    if (!pattern.test(inputChar)) {
      $event.preventDefault();
    }
  }

  getBase64(img: File, callback: (img: {}) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

}
