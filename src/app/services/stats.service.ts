import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SingleStats, Statistique} from '@app/models/Stats';

@Injectable()
export class StatsService {
  colors = [
    '#3366CC',
    '#DC3912',
    '#FF9900',
    '#109618',
    '#990099',
    '#3B3EAC',
    '#0099C6',
    '#DD4477',
    '#66AA00',
    '#B82E2E',
    '#316395',
    '#994499',
    '#22AA99',
    '#AAAA11',
    '#6633CC',
    '#E67300',
    '#8B0707',
    '#329262',
    '#5574A6',
    '#3B3EAC',
  ];

  constructor(
    private http: HttpClient
  ) {
  }

  getCongeStatsByDays(days: number) {
    return this.http.get<CongeStats>(`/conges/stats?days=${days}`);
  }

  getCurrentUserCongeStatsByDays(days: number) {
    return this.http.get<CongeStats>(`/conges/stats/me?days=${days}`);
  }

  getAbsenceStatsByDays(days: number) {
    return this.http.get<AbsenceStats>(`/absences/stats?days=${days}`);
  }

  getCurrentUserAbsenceStatsByDays(days: number) {
    return this.http.get<AbsenceStats>(`/absences/stats/me?days=${days}`);
  }

  getCongeStatsByDate(beginDate: String, endDate: String) {
    return this.http.get<CongeStats>(`/conges/stats?beginDate=${beginDate}&endDate=${endDate}`);
  }

  getCurrentUserCongeStatsByDate(beginDate: String, endDate: String) {
    return this.http.get<CongeStats>(`/conges/stats/me?beginDate=${beginDate}&endDate=${endDate}`);
  }

  getAbsencestatsByDate(beginDate: String, endDate: String) {
    return this.http.get<AbsenceStats>(`/absences/stats?beginDate=${beginDate}&endDate=${endDate}`);
  }

  getCurrentUserAbsencestatsByDate(beginDate: String, endDate: String) {
    return this.http.get<AbsenceStats>(`/absences/stats/me?beginDate=${beginDate}&endDate=${endDate}`);
  }

  getEmployeesStats() {
    return this.http.get<Statistique>('/employees/stats/all');
  }

  getSingleEmployeStats() {
    return this.http.get<SingleStats>('/employees/stats');
  }
}
