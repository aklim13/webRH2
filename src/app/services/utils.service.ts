import {Injectable} from '@angular/core';

@Injectable()
export class UtilsService {

  constructor() {
  }

  getIds(ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return result.slice(0, -1);
  }
}
