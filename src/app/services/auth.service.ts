import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import {TokenService} from '@app/services/token.service';


@Injectable()
export class AuthService {

  constructor(private http: HttpClient,
              private router: Router,
              private tokenService: TokenService) {
  }


  login(username: string, password: string) {
    return this.http.post<any>(`/auth/signin`, {usernameOrEmail: username, password: password})
      .map(user => {
        this.tokenService.saveToken(user);
        return user;
      });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.clear();
    this.router.navigate(['/']);

  }
}
