import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class RefreshProfileService {
  private historySource = new BehaviorSubject<any>({});
  refreshHistory = this.historySource.asObservable();

  private documentSource = new BehaviorSubject<any>({});
  refreshDocument = this.documentSource.asObservable();

  private infoSource = new BehaviorSubject<any>({});
  refreshInfo = this.infoSource.asObservable();

  constructor() {
  }

  changeHistory() {
    this.historySource.next({});
  }

  changeDocument() {
    this.documentSource.next({});
  }

  changeInfo() {
    this.infoSource.next({});
  }
}
