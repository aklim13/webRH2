import {Injectable} from '@angular/core';
import {Page} from '@app/models/Page';
import {HistoriqueList} from '@app/models/lists/HistoriqueList';
import {HttpClient} from '@angular/common/http';
import {EmployeEditData} from '@app/models/EmployeEditData';
import {Prime} from '@app/models/Prime';
import {UtilsService} from '@app/services/utils.service';

@Injectable()
export class PrimeService {

  constructor(private http: HttpClient, private utils: UtilsService) {
  }

  getUserPrime(matricule: number, page: Page) {
    return this.http.get<HistoriqueList>(`/profile/${matricule}/primes?page=${page.pageNumber}&size=${page.size}`);
  }

  getPrimes(matricule: number) {
    return this.http.get<Prime[]>(`/profile/${matricule}/primes/new`);
  }

  postPrime(matricule: number, primeId: number) {
    return this.http.post(`/profile/${matricule}/primes/${primeId}`, '');
  }

  deletePrime(matricule: number, ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return this.http.delete(`/profile/${matricule}/primes/${result.slice(0, -1)}`);
  }

  updatePrime(matricule: number, primeId: number, empPrimeId: number) {
    return this.http.put<EmployeEditData>(`/profile/${matricule}/primes/${empPrimeId}/${primeId}`, '');
  }

  getPrimesPageable(page: Page, query: string) {
    return this.http.get(`/settings/primes?page=${page.pageNumber}&size=${page.size}`);
  }

  addPrime(prime: Prime) {
    return this.http.post('/settings/primes', prime);
  }

  deletePrimes(ids: number[]) {
    return this.http.delete('/settings/primes/' + this.utils.getIds(ids));
  }

  modifyPrime(prime: Prime) {
    return this.http.put('/settings/primes/' + prime.primeId, prime);
  }
}
