import {inject, TestBed} from '@angular/core/testing';

import {RefreshProfileService} from './refresh-profile.service';

describe('RefreshProfileService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RefreshProfileService]
    });
  });

  it('should be created', inject([RefreshProfileService], (service: RefreshProfileService) => {
    expect(service).toBeTruthy();
  }));
});
