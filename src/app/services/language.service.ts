import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Niveau} from '@app/models/Niveau';
import {Language} from '@app/models/Language';
import {LanguageLevelDTO} from '@app/models/LanguageLevelDTO';
import {UtilsService} from '@app/services/utils.service';
import {Page} from '@app/models/Page';

@Injectable()
export class LanguageService {

  constructor(private http: HttpClient, private utils: UtilsService) {
  }

  getUserLanguages(matricule: number) {
    return this.http.get<Language[]>(`/profile/${matricule}/languages/new`);
  }

  getLanguages(page: Page, query: string) {
    return this.http.get<Language[]>(`/settings/languages?page=${page.pageNumber}&size=${page.size}`);
  }

  getLevels() {
    return this.http.get<Niveau[]>(`/settings/levels`);
  }

  addLanguages(language: string) {
    return this.http.post('/settings/languages/', {language: language});
  }

  addLanguageToUser(language: LanguageLevelDTO, matricule: number) {
    return this.http.post(`/profile/${matricule}/languages`, language);
  }

  updateLanguages(language: Language) {
    return this.http.put('/settings/languages/' + language.langueId, {language: language.language});
  }

  updateUserLanguage(language: LanguageLevelDTO, matricule: number) {
    return this.http.put(`/profile/${matricule}/languages/${language.languageLevelId}`, language);
  }

  deleteLanguages(ids: number[]) {
    return this.http.delete('/settings/languages/' + this.utils.getIds(ids));
  }

  deleteUserLanguage(matricule: number, ids: number[]) {
    return this.http.delete(`/profile/${matricule}/languages/${this.utils.getIds(ids)}`);
  }

}
