import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Page} from '@app/models/Page';
import {ExperienceList} from '@app/models/lists/ExperienceList';
import {ContactList} from '@app/models/lists/ContactList';
import {DocumentList} from '@app/models/lists/DocumentList';
import {HistoriqueList} from '@app/models/lists/HistoriqueList';
import {PrimeList} from '@app/models/PrimeList';
import {LanguageList} from '@app/models/LanguageList';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  getUserLanguages(matricule: number, page: Page) {
    return this.http.get<LanguageList>(`/profile/${matricule}/languages?page=${page.pageNumber}&size=${page.size}`);
  }

  getUserHistorique(matricule: number, query: string, page: Page) {
    return this.http.get<HistoriqueList>(`/profile/${matricule}/historiques?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getUserPrime(matricule: number, query: string, page: Page) {
    return this.http.get<PrimeList>(`/profile/${matricule}/primes?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getUserExperiences(matricule: number, query: string, page: Page) {
    return this.http.get<ExperienceList>(`/profile/${matricule}/experiences?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getUserDocuments(matricule: number, query: string, page: Page) {
    return this.http.get<DocumentList>(`/documents/${matricule}/users?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getUserContacts(matricule: number, query: string, page: Page) {
    return this.http.get<ContactList>(`/profile/${matricule}/contacts?page=${page.pageNumber}&size=${page.size}&q=${query}`);
  }

  getSelectedEmploye(matricule: number) {
    return this.http.get(`/employees/${matricule}`);
  }

  downloadDocument(lien: string) {
    return this.http.get(lien, {responseType: 'blob'});
  }
}
