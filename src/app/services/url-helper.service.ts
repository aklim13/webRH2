import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UrlHelperService {
  constructor(
    private http: HttpClient, /* Your favorite Http requester */
  ) {
  }

  get(url: string): Observable<string> {
    return new Observable((observer) => {
      let objectUrl: string = null;

      this.http
        .get(url, {
          responseType: 'blob'
        })
        .subscribe(m => {
          objectUrl = URL.createObjectURL(m);
          observer.next(objectUrl);
        });

      return () => {
        if (objectUrl) {
          URL.revokeObjectURL(objectUrl);
          objectUrl = null;
        }
      };
    });
  }

}
