import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Experience} from '@app/models/Experience';

@Injectable()
export class ExperienceService {

  constructor(private http: HttpClient) {
  }

  addExperience(exp: Experience, matricule: number) {
    return this.http.post(`/profile/${matricule}/experiences`, exp);
  }

  updateExperience(exp: Experience, matricule: number) {
    return this.http.put(`/profile/${matricule}/experiences/${exp.experienceId}`, exp);
  }

  deleteExperience(matricule: number, ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return this.http.delete(`/profile/${matricule}/experiences/${result.slice(0, -1)}`);
  }
}
