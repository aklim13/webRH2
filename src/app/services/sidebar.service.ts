import {Injectable} from '@angular/core';
import {UserService} from '@app/services/user.service';
import {NavList} from '@app/models/NavList';

@Injectable()
export class SidebarService {
  adminNavList: NavList[] = [];
  employeNavList: NavList[] = [];
  profileNavs: NavList = {} as NavList;

  constructor(private userService: UserService) {
    this.adminNavList = [
      {
        icon: 'pie_chart', title: 'Dashboard', mono: true, link: '/dashboard'
      },
      {
        icon: 'group_add', link: '/employees', title: 'Employées', mono: true
      },
      {
        icon: 'settings', link: '/configuration', title: 'Configuration', mono: true
      },
      {
        icon: 'event_available', title: 'Congé', subtitle: [
          {icon: 'find_replace', link: '/conge/demandes', title: 'Demandes'},
          {icon: 'calendar_today', link: '/conge/historique', title: 'Historique'},
          {icon: 'show_chart', title: 'Statistique', link: '/conge/statistique'}
        ]
      },
      {
        icon: 'event_available', title: 'Absences', subtitle: [
          {icon: 'edit', link: '/absence/noter', title: 'Noter'},
          {icon: 'edit', link: '/presence', title: 'Presence'},
          {icon: 'date_range', link: '/absence/historique', title: 'Historique'},
          {icon: 'show_chart', title: 'Statistique', link: '/absence/statistique'}
        ]
      },
    ];
    this.employeNavList = [
      {
        icon: 'pie_chart', title: 'Dashboard', mono: true, link: '/dashboard'
      },
      {
        icon: 'event_available', title: 'Congé', subtitle: [
          {icon: 'calendar_today', title: 'Mes congés', link: '/conge/demandes/me'},
          {icon: 'show_chart', title: 'Statistique', link: '/conge/statistique'},
        ]
      },
      {
        icon: 'person', title: 'Absence', subtitle: [
          {icon: 'date_range', link: '/absence/historique/me', title: 'Mes absences'},
          {icon: 'show_chart', title: 'Statistique', link: '/absence/statistique'},
        ]
      }
    ];
    this.profileNavs = {
      icon: 'person', title: 'Profile', subtitle: [
        {title: 'Information', icon: 'person', link: '#info'},
        {title: 'Document', icon: 'attach_file', link: '#document'},
        {title: 'Prime', icon: 'keyboard_arrow_up', link: '#prime'},
        {title: 'Contact', icon: 'call', link: '#contact'},
        {title: 'Competence', icon: 'stars', link: '#competence'},
        {title: 'Experience', icon: 'work', link: '#exp'},
        {title: 'Langue', icon: 'language', link: '#lang'},
        {title: 'Historique', icon: 'history', link: '#historique'},
      ]
    };
  }

  getCurrentUserNav() {
    switch (this.userService.getCurrentUserRole()) {
      case 'admin':
        return this.adminNavList;
      case 'employe':
        return this.employeNavList;
    }
  }
}
