import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest} from '@angular/common/http';
import {TypeDocument} from '@app/models/TypeDocument';

@Injectable()
export class DocumentService {

  constructor(private http: HttpClient) {
  }

  getTypeDocuments() {
    return this.http.get<TypeDocument[]>(`/documents/typeDocuments`);
  }

  uploadDocument(uploadedFiles) {
    const formData = new FormData();
    uploadedFiles.map(file => {
      formData.append('doc', file.title.toUpperCase());
      formData.append('file', file.file);
    });
    const request = new HttpRequest('POST', '/documents/upload/multiple', formData, {reportProgress: true});
    return this.http.request(request);
  }

  deleteDocument(ids: number[]) {
    let result = '';
    ids.forEach(id => {
      result += (id + ',');
    });
    return this.http.delete(`/documents/${result.slice(0, -1)}`);
  }
}
