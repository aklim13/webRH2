import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    // const baseUrl = 'http://192.168.1.110:8080/api';
    const baseUrl = 'http://localhost:8080/api';
    // const baseUrl = 'http://192.168.1.108:8080/api';
    const token = localStorage.getItem('token');
    if (token) {
      request = request.clone({
        url: baseUrl + request.url,
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    } else {
      request = request.clone({
        url: baseUrl + request.url
      });
    }
    return next.handle(request);
  }
}
